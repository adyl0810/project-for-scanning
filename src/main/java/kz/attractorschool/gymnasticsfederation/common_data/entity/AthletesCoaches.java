package kz.attractorschool.gymnasticsfederation.common_data.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.CoachRole;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Table(name = "athletes_coaches")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AthletesCoaches {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JsonBackReference
    @NotNull
    private Athlete athlete;

    @OneToOne
    @JsonBackReference
    @NotNull
    private Coach coach;

    @OneToOne
    @JsonBackReference
    @NotNull
    private School school;

    @Column
    @NotNull
    private String role;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private LocalDate registerDate;

    @Column
    private LocalDate finishDate;

    @Column
    @Builder.Default
    private boolean isDel = false;
}
