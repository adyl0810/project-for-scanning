package kz.attractorschool.gymnasticsfederation.common_data.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "competition_orders")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompetitionOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @ToString.Exclude
    private Athlete athlete;

    @ManyToOne
    @ToString.Exclude
    private AthletesTeam athletesTeam;

    @ManyToOne
    @ToString.Exclude
    private Competition competition;

    @ManyToOne
    @ToString.Exclude
    private DisciplineType discipline;

    @ManyToOne
    @ToString.Exclude
    private AgeCategory ageCategory;

    @Column
    private Integer flow;

    @Column
    private Integer number;
}
