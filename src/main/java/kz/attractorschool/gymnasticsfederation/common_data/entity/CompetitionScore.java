package kz.attractorschool.gymnasticsfederation.common_data.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.ScoreStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Table(name = "competition_scores")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompetitionScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private double score;

    @Column
    private String status = ScoreStatus.ACTIVE.getName();

//    @Column
//    private double deduction;
//
//    @Column
//    private double sum;
//
//    @Column
//    private int place;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private ScoreCriterion criterion;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private CompetitionDiscipline disciplineType;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private CompetitionDisciplineProgram program;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private CompetitionDisciplineAge ageCategory;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private ParticipationApplicationAthlete athlete;

//    @JsonManagedReference
//    @ManyToOne
//    @ToString.Exclude
//    private Judge judge;
}
