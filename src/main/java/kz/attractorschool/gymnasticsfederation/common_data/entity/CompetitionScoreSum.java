package kz.attractorschool.gymnasticsfederation.common_data.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "competition_score_sums")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompetitionScoreSum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private double sum;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private CompetitionDiscipline disciplineType;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private CompetitionDisciplineProgram program;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private CompetitionDisciplineAge ageCategory;

    @JsonManagedReference
    @ManyToOne
    @ToString.Exclude
    private ParticipationApplicationAthlete athlete;
}
