package kz.attractorschool.gymnasticsfederation.common_data.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.DopingFile;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.JudgeCategoryFile;
import lombok.*;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "judges")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Judge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ToString.Exclude
    @ManyToOne
    private Person person;

    @ToString.Exclude
    @ManyToOne
    @JsonBackReference
    private School school;

    @Column
    @NotNull
    @Builder.Default
    private String registryNumber = "";

    @ToString.Exclude
    @ManyToOne
    private Discipline discipline;

    @ToString.Exclude
    @ManyToOne
    private JudgeCategory category;

    @ToString.Exclude
    @OneToOne
    private JudgeCategoryFile categoryFile;

    @Column
    @NotNull
    @Builder.Default
    private boolean isDel = false;
}
