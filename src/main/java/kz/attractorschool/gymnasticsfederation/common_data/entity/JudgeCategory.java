package kz.attractorschool.gymnasticsfederation.common_data.entity;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "judge_categories")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JudgeCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    @Builder.Default
    private boolean isDel = false;
}
