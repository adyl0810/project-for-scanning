package kz.attractorschool.gymnasticsfederation.common_data.entity;

import kz.attractorschool.gymnasticsfederation.common_data.enumiration.SchoolChangeStatus;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "school_changes")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SchoolChange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Builder.Default
    private LocalDate date = LocalDate.now();

    @Column
    @Builder.Default
    private String status = SchoolChangeStatus.FILED.getName();

    @ManyToOne
    private School oldSchool;

    @ManyToOne
    private School newSchool;

    @ManyToOne
    private Athlete athlete;

    @ManyToOne
    private Coach coach;

    @ManyToOne
    private Judge judge;
}
