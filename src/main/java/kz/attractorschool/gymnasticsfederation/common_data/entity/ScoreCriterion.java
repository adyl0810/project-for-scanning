package kz.attractorschool.gymnasticsfederation.common_data.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.ScoreStatus;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "score_criteria")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ScoreCriterion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private String name;

    @Column
    private Integer minScore;

    @Column
    private Integer maxScore;

    @Column
    @NotNull
    @Builder.Default
    private boolean isDel = false;

    @ToString.Exclude
    @ManyToOne
    @JsonBackReference
    private Discipline discipline;

    @ToString.Exclude
    @ManyToOne
    @JsonBackReference
    private DisciplineType disciplineType;
}
