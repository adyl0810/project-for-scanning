package kz.attractorschool.gymnasticsfederation.common_data.entity;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "score_criterion_judges")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ScoreCriterionJudge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @NotNull
    private ParticipationApplicationJudge judge;

    @ManyToOne
    @NotNull
    private ScoreCriterion criterion;
}
