package kz.attractorschool.gymnasticsfederation.common_data.entity;

import kz.attractorschool.gymnasticsfederation.common_data.enumiration.Role;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Set;

@Table(name = "users")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
//    @Email(message = "Введите корректный мейл")
    private String email;

    @Column
    @NotNull
    private String password;

    @Column
    @NotNull
    private String role;

    @Column
    @NotNull
    @Builder.Default
    private boolean active = true;
}
