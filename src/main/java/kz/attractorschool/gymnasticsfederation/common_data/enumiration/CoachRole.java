package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CoachRole {
    MAIN_COACH("Личный тренер"), SECOND_COACH("Второй тренер"), Choreograph("Хореограф");
    private String name;
}
