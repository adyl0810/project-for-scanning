package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CompetitionLevel {
    INTERNATIONAL("Международный"), COUNTRY("Республиканский"), REGIONAL("Региональный");

    private final String name;
}
