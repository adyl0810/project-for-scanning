package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CompetitionStatus {
    UNDER_CONSIDERATION("На_рассмотрении"), CONFIRMED("Подтверждено"), COMPLETED("Завершено");

    private final String name;
}
