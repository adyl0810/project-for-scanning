package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ParticipationApplicationStatus {
    CREATED("Создана"), FILED("Подана"), CONFIRMED("Подтверждена");

    private final String name;
}
