package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    FEDERATION, SCHOOL, ADMIN, SECRETARY;

    @Override
    public String getAuthority() {
        return name();
    }
}
