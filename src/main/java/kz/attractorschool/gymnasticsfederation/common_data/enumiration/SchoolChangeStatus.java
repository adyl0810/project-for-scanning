package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SchoolChangeStatus {
    FILED("Подана"), CONFIRMED("Подтверждена"), CANCELED("Отменена");
    private String name;
}
