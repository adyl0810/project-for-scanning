package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ScoreStatus {
    CANCELED("Аннулирована"), REVISED("Пересмотрена"), EXCLUDED_FROM_RESULT("Исключена из результата"),
    ACTIVE("Активная");

    private String name;
}
