package kz.attractorschool.gymnasticsfederation.common_data.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Status {
    ACTIVE("Активный"), UNDER_CONSIDERATION("На_рассмотрении"), EXPIRED("Истек"),
    INACTIVE("Неактивный"), DISQUALIFIED("Дисквалифицирован");

    private final String name;
}

