package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Athlete;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AthleteRepository extends JpaRepository<Athlete, Integer>, JpaSpecificationExecutor<Athlete> {
    List<Athlete> findAllBySchoolId(Integer id);
    List<Athlete> findAllBySchoolIdAndDisciplineId(Integer schoolId, Integer disciplineId);
}
