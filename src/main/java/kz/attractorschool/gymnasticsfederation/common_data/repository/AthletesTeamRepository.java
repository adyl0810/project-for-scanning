package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.AthletesTeam;
import kz.attractorschool.gymnasticsfederation.common_data.entity.ParticipationApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AthletesTeamRepository extends JpaRepository<AthletesTeam, Integer> {
    List<AthletesTeam> findAllByCompetitionId(int id);
    boolean existsByApplicationIdAndNumber(int application, int number);
    List<AthletesTeam> findByApplicationIdAndNumber(int application, int number);
}
