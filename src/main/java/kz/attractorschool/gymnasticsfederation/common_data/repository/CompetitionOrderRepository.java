package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionOrder;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompetitionOrderRepository extends JpaRepository<CompetitionOrder, Integer> {
    List<CompetitionOrder> findAllByCompetitionIdOrderByFlow(int id, Sort sort);
    List<CompetitionOrder> findAllByCompetitionId(int id);
    List<CompetitionOrder> findAllByCompetitionId(int id, Sort sort);
    List<CompetitionOrder> findAllByCompetitionIdAndFlow(int competitionId, int flow);
    Optional<CompetitionOrder> findByCompetitionIdAndAthleteId(int competitionId, int athleteId);
    boolean existsByCompetitionId(int id);
}
