package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetitionScoreRepository extends JpaRepository<CompetitionScore, Integer> {
    CompetitionScore findByAgeCategoryIdAndAthleteIdAndProgramIdAndCriterionIdAndDisciplineTypeId(Integer ageCategoryId, Integer athleteId, Integer programId, Integer criterionId, Integer disciplineTypeId);
}
