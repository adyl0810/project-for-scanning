package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScoreSum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitionScoreSumRepository extends JpaRepository<CompetitionScoreSum, Integer> {
    CompetitionScoreSum findByDisciplineTypeIdAndAgeCategoryIdAndProgramIdAndAthleteId(Integer disciplineTypeId, Integer ageCategoryId, Integer programId, Integer athleteId);
}
