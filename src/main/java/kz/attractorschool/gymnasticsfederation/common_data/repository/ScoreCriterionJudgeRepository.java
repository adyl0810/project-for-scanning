package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterionJudge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScoreCriterionJudgeRepository extends JpaRepository<ScoreCriterionJudge, Integer> {
    List<ScoreCriterionJudge> findAllByJudgeApplicationCompetitionIdOrderByCriterionName(Integer id);
}
