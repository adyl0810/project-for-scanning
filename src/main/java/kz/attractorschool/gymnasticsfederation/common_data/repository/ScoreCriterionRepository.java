package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScoreCriterionRepository extends JpaRepository<ScoreCriterion, Integer> {
    List<ScoreCriterion> findAllByDisciplineId(Integer id);
}
