package kz.attractorschool.gymnasticsfederation.common_data.repository;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Secretary;
import kz.attractorschool.gymnasticsfederation.common_data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SecretaryRepository extends JpaRepository<Secretary, Integer> {
    Optional<Secretary> findByUser(User user);
    List<Secretary> findBySchoolId(int id);
}
