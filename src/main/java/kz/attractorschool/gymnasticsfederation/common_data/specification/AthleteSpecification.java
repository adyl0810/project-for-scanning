package kz.attractorschool.gymnasticsfederation.common_data.specification;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Athlete;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Person;
import kz.attractorschool.gymnasticsfederation.dto.search.AthleteFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class AthleteSpecification implements ModelSpecification<Athlete, AthleteFilter>{

    public static Specification<Athlete> joinTest(String input) {
        return new Specification<Athlete>() {
            public Predicate toPredicate(Root<Athlete> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Join<Athlete,Person> userProd = root.join("person");
                return cb.equal(userProd.get("name"), input);
            }
        };
    }

    @Override
    public Specification<Athlete> createSpecification(AthleteFilter filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getName())) {
                    Join<Athlete,Person> personProd = root.join("person");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("name")), "%" + filter.getName().toLowerCase() + "%"));
                }
                if (!isEmpty(filter.getSurname())) {
                    Join<Athlete,Person> personProd = root.join("person");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("surname")), "%" + filter.getSurname().toLowerCase() + "%"));
                }
                if (isEmpty(filter.getCity())) {
                    Join<Athlete,Person> personProd = root.join("person");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("city")), "%" + filter.getCity().toLowerCase() + "%"));
                }
                if(!isEmpty(filter.getRegistryNum())){
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("registryNumber")), "%" + filter.getRegistryNum().toLowerCase() + "%"));
                }
                if (filter.getDisciplineId() > 0) {
                    predicates.add(root.join("discipline", JoinType.LEFT).in(Collections.singletonList(filter.getDisciplineId())));
                }
                if (filter.getRankId() > 0) {
                    predicates.add(root.join("rank", JoinType.LEFT).in(Collections.singletonList(filter.getRankId())));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
