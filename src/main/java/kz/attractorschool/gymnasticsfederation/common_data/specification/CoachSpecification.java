package kz.attractorschool.gymnasticsfederation.common_data.specification;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Coach;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Person;
import kz.attractorschool.gymnasticsfederation.dto.search.CoachFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class CoachSpecification implements ModelSpecification<Coach, CoachFilter>{
//    private final PersonRepository personRepository;

    @Override
    public Specification<Coach> createSpecification(CoachFilter filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getName())) {
                    Join<Coach,Person> personProd = root.join("person");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("name")), "%" + filter.getName().toLowerCase() + "%"));
                }
                if (!isEmpty(filter.getSurname())) {
                    Join<Coach,Person> personProd = root.join("person");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("surname")), "%" + filter.getSurname().toLowerCase() + "%"));
                }
                if (isEmpty(filter.getCity())) {
                    Join<Coach,Person> personProd = root.join("person");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("city")), "%" + filter.getCity().toLowerCase() + "%"));
                }
                if(!isEmpty(filter.getRegistryNum())){
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("registryNumber")), "%" + filter.getRegistryNum().toLowerCase() + "%"));
                }
                if (filter.getDisciplineId() > 0) {
                    predicates.add(root.join("discipline", JoinType.LEFT).in(Collections.singletonList(filter.getDisciplineId())));
                }
                if (filter.getCategoryId() > 0) {
                    predicates.add(root.join("category", JoinType.LEFT).in(Collections.singletonList(filter.getCategoryId())));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public void sorting(Root<Coach> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder, SortingModel sorting) {
        ModelSpecification.super.sorting(root, criteriaQuery, criteriaBuilder, sorting);
    }
}
