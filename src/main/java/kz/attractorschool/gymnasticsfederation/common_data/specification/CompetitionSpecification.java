package kz.attractorschool.gymnasticsfederation.common_data.specification;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Competition;
import kz.attractorschool.gymnasticsfederation.dto.search.CompetitionFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class CompetitionSpecification implements ModelSpecification<Competition, CompetitionFilter>{
    @Override
    public Specification<Competition> createSpecification(CompetitionFilter filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getName())) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + filter.getName().toLowerCase() + "%"));
                }
                if (filter.getStartDate() != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("startDate")), "%" + filter.getStartDate().toString() + "%"));
                }
                if (filter.getApplicationDate() != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("participationDate")), "%" + filter.getApplicationDate().toString() + "%"));
                }
                if (!isEmpty(filter.getCity())) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("city")), "%" + filter.getCity().toLowerCase() + "%"));
                }
                if (filter.getDisciplineId() > 0) {
                    predicates.add(root.join("discipline", JoinType.LEFT).in(Collections.singletonList(filter.getDisciplineId())));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
