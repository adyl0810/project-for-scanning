package kz.attractorschool.gymnasticsfederation.common_data.specification;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Judge;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Person;
import kz.attractorschool.gymnasticsfederation.dto.search.JudgeFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class JudgeSpecification implements ModelSpecification<Judge, JudgeFilter>{
//    private final PersonRepository personRepository;

    @Override
    public Specification<Judge> createSpecification(JudgeFilter filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (!isEmpty(filter.getName())) {
                Join<Judge, Person> personProd = root.join("person");
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("name")), "%" + filter.getName().toLowerCase() + "%"));
            }
            if (!isEmpty(filter.getSurname())) {
                Join<Judge,Person> personProd = root.join("person");
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("surname")), "%" + filter.getSurname().toLowerCase() + "%"));
            }
            if (isEmpty(filter.getCity())) {
                Join<Judge,Person> personProd = root.join("person");
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("city")), "%" + filter.getCity().toLowerCase() + "%"));
            }
            if(!isEmpty(filter.getRegistryNum())){
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("registryNumber")), "%" + filter.getRegistryNum().toLowerCase() + "%"));
            }
            if (filter.getDisciplineId() > 0) {
                predicates.add(root.join("discipline", JoinType.LEFT).in(Collections.singletonList(filter.getDisciplineId())));
            }
            if (filter.getCategoryId() > 0) {
                predicates.add(root.join("category", JoinType.LEFT).in(Collections.singletonList(filter.getCategoryId())));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public void sorting(Root<Judge> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder, SortingModel sorting) {
        ModelSpecification.super.sorting(root, criteriaQuery, criteriaBuilder, sorting);
    }
}
