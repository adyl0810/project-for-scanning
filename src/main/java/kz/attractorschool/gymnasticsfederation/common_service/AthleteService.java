package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.CoachRole;
import kz.attractorschool.gymnasticsfederation.common_data.repository.*;
import kz.attractorschool.gymnasticsfederation.common_data.specification.ModelSpecification;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import kz.attractorschool.gymnasticsfederation.dto.*;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.Status;
import kz.attractorschool.gymnasticsfederation.dto.add.AthleteAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.add.AthleteCoachAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.search.AthleteFilter;
import kz.attractorschool.gymnasticsfederation.dto.update.AthleteUpdateDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.DopingFile;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.MedicalFile;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.RankFile;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.RegistryFile;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@AllArgsConstructor
public class AthleteService {
    private final AthleteRepository repository;
    private final SchoolService schoolService;
    private final RankService rankService;
    private final DisciplineService disciplineService;
    private final PersonService personService;
    private final DopingFileRepository dopingFileRepository;
    private final RankFileRepository rankFileRepository;
    private final MedicalFileRepository medicalFileRepository;
    private final RegistryFileRepository registryFileRepository;
    private final CoachService coachService;
    private final AthletesCoachesRepository athletesCoachesRepository;
    private final AgeCategoryService ageCategoryService;
    private final ModelSpecification<Athlete, AthleteFilter> specification;

    public List<Athlete> all() {
        List<Athlete> athletes = repository.findAll();
        return checkStatus(athletes);
    }

    public List<Athlete> findAllBySchoolId(Integer id) {
        List<Athlete> athletes = repository.findAllBySchoolId(id);
        return checkStatus(athletes);
    }

    public List<Athlete> findAllBySchoolAndDisciplineId(Integer schoolId, Integer disciplineId) {
        List<Athlete> athletes = repository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
        return checkStatus(athletes);
    }

    public List<Athlete> findAllBySchoolAndDisciplineAndCityTeam(Integer schoolId, Integer disciplineId) {
        List<Athlete> athletes = repository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
        List<Athlete> athletesCity = new ArrayList<>();
        for (Athlete athlete : athletes) {
            if (athlete.isCityTeam()) {
                athletesCity.add(athlete);
            }
        }
        return checkStatus(athletesCity);
    }

    public List<Athlete> findAllBySchoolAndDisciplineAndNationalTeam(Integer schoolId, Integer disciplineId) {
        List<Athlete> athletes = repository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
        List<Athlete> athletesCity = new ArrayList<>();
        for (Athlete athlete : athletes) {
            if (athlete.isNationalTeam()) {
                athletesCity.add(athlete);
            }
        }
        return checkStatus(athletesCity);
    }

    public List<Athlete> findAllBySchoolAndDisciplineAndAgeCategory(Integer schoolId, Integer disciplineId, Integer ageCategoryId) {
        List<Athlete> athletes = repository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
        AgeCategory ageCategory = ageCategoryService.findOne(ageCategoryId);
        List<Athlete> athletesFiltered = new ArrayList<>();
        for (Athlete athlete : athletes) {

            if (ageCategory.getRank() != null && ageCategory.getMaxYear() != null && ageCategory.getMinYear() != null) {
                if (athlete.getRank() == ageCategory.getRank() && athlete.getPerson().getBirthday().getYear() < ageCategory.getMinYear() && athlete.getPerson().getBirthday().getYear() > ageCategory.getMaxYear()) {
                    athletesFiltered.add(athlete);
                }
            }

            if (ageCategory.getRank() != null && ageCategory.getMaxYear() != null && ageCategory.getMinYear() == null) {
                if (athlete.getRank() == ageCategory.getRank() && athlete.getPerson().getBirthday().getYear() > ageCategory.getMaxYear()) {
                    athletesFiltered.add(athlete);
                }
            }

            if (ageCategory.getRank() != null && ageCategory.getMaxYear() == null && ageCategory.getMinYear() != null) {
                if (athlete.getRank() == ageCategory.getRank() && athlete.getPerson().getBirthday().getYear() < ageCategory.getMinYear()) {
                    athletesFiltered.add(athlete);
                }
            }

            if (ageCategory.getRank() == null && ageCategory.getMaxYear() != null && ageCategory.getMinYear() != null) {
                if (athlete.getPerson().getBirthday().getYear() < ageCategory.getMinYear() && athlete.getPerson().getBirthday().getYear() > ageCategory.getMaxYear()) {
                    athletesFiltered.add(athlete);
                }
            }

            if (ageCategory.getRank() != null && ageCategory.getMaxYear() == null && ageCategory.getMinYear() == null) {
                if (athlete.getRank() == ageCategory.getRank()) {
                    athletesFiltered.add(athlete);
                }
            }

            if (ageCategory.getRank() == null && ageCategory.getMaxYear() != null && ageCategory.getMinYear() == null) {
                if (athlete.getPerson().getBirthday().getYear() > ageCategory.getMaxYear()) {
                    athletesFiltered.add(athlete);
                }
            }

            if (ageCategory.getRank() == null && ageCategory.getMaxYear() == null && ageCategory.getMinYear() != null) {
                if (athlete.getPerson().getBirthday().getYear() < ageCategory.getMinYear()) {
                    athletesFiltered.add(athlete);
                }
            }
        }
        return checkStatus(athletesFiltered);
    }

    public Athlete findOne(Integer id) {
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Спортсмен", id);
        });
    }

    public AthleteDTO getOne(Integer id) {
        return AthleteDTO.from(repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Спортсмен", id);
        }));
    }

    public AthleteDTO add(AthleteAddDTO athleteDTO, RegistryFile registryFile,
                          MedicalFile medicalFile, RankFile rankFile,
                          DopingFile dopingFile) {
        RegistryFile registry = registryFileRepository.save(registryFile);
        MedicalFile medical = medicalFileRepository.save(medicalFile);
        RankFile rankFile2 = rankFileRepository.save(rankFile);
        DopingFile doping = dopingFileRepository.save(dopingFile);
        School school = schoolService.findOne(athleteDTO.getSchoolId());
        Discipline discipline = disciplineService.findOne(athleteDTO.getDisciplineId());
        Person person = personService.findOne(athleteDTO.getPersonId());
        Athlete athlete = repository.save(Athlete.builder()
                .person(person)
                .school(school)
                .isCityTeam(isTeam(athleteDTO.getIsCityTeam()))
                .isNationalTeam(isTeam(athleteDTO.getIsNationalTeam()))
                .registryFile(registry)
                .medicalFile(medical)
                .dopingFile(doping)
                .registryDate(personService.dateFromString(athleteDTO.getRegistryDate()))
                .discipline(discipline)
                .rankFile(rankFile2)
                .build());
        if (athleteDTO.getRankId() > 0) {
            Rank rank = rankService.findOne(athleteDTO.getRankId());
            athlete.setRank(rank);
        }
        String registryNum = "A-" + personService.addRegistryNumber(person.getGender(), discipline)
                + personService.countIdForRegistryNum(athlete.getId());
        athlete.setRegistryNumber(registryNum);
        repository.save(athlete);
        return AthleteDTO.from(athlete);
    }

    public String delete(Integer id) {
        Athlete athlete = findOne(id);
        athlete.setDel(true);
        repository.save(athlete);
        return "ok";
    }

    public AthleteDTO update(Integer id, AthleteUpdateDTO athleteDTO) {
        Athlete athlete = findOne(id);
        Rank rank = rankService.findOne(athleteDTO.getRankId());
        Discipline discipline = disciplineService.findOne(athleteDTO.getDisciplineId());
        athlete.setDiscipline(discipline);
        athlete.setRank(rank);
        athlete.setNationalTeam(isTeam(athleteDTO.getIsNationalTeam()));
        athlete.setCityTeam(isTeam(athleteDTO.getIsCityTeam()));
        athlete.setRegistryDate(personService.dateFromString(athleteDTO.getRegistryDate()));
        repository.save(athlete);
        return AthleteDTO.from(athlete);
    }

    public Athlete updateFile(Integer id, RegistryFile registryFile) {
        Athlete athlete = findOne(id);
        RegistryFile registry = registryFileRepository.save(registryFile);
        athlete.setRegistryFile(registry);
        return athlete;
    }

    public Athlete updateFile(Integer id, MedicalFile medicalFile) {
        Athlete athlete = findOne(id);
        MedicalFile medical = medicalFileRepository.save(medicalFile);
        athlete.setMedicalFile(medical);
        return athlete;
    }

    public Athlete updateFile(Integer id, RankFile rankFile) {
        Athlete athlete = findOne(id);
        RankFile rankFile2 = rankFileRepository.save(rankFile);
        athlete.setRankFile(rankFile2);
        return athlete;
    }

    public Athlete updateFile(Integer id, DopingFile dopingFile) {
        Athlete athlete = findOne(id);
        DopingFile doping = dopingFileRepository.save(dopingFile);
        athlete.setDopingFile(doping);
        return athlete;
    }

    public boolean isPdf(MultipartFile multipartFile) {
        String name = multipartFile.getOriginalFilename();
        String[] words = name.split("\\.");
        String format = words[words.length - 1];
        return format.equals("pdf");
    }

    public AthleteDTO confirm(Integer id) {
        Athlete athlete = findOne(id);
        athlete.setStatus(Status.ACTIVE.getName());
        repository.save(athlete);
        return AthleteDTO.from(athlete);
    }

    public List<Athlete> checkStatus(List<Athlete> athletes) {
        for (int i = 0; i < athletes.size(); i++) {
            if (athletes.get(i).getRegistryDate().plusYears(1).isBefore(LocalDate.now())) {
                athletes.get(i).setStatus(Status.EXPIRED.getName());
                repository.save(athletes.get(i));
            } else if (athletes.get(i).getRegistryDate().plusMonths(14).isBefore(LocalDate.now())) {
                athletes.get(i).setStatus(Status.INACTIVE.getName());
                repository.save(athletes.get(i));
            }
        }
        return athletes;
    }

    public AthleteDTO checkStatus(Athlete athlete) {
        if (athlete.getRegistryDate().plusMonths(12).isBefore(LocalDate.now())) {
            athlete.setStatus(Status.EXPIRED.getName());
            repository.save(athlete);
        } else if (athlete.getRegistryDate().plusMonths(14).isBefore(LocalDate.now())) {
            athlete.setStatus(Status.INACTIVE.getName());
            repository.save(athlete);
        }
        return AthleteDTO.from(athlete);
    }

    public AthleteDTO register(Integer id, AthleteRegisterDTO athleteDTO, MedicalFile medicalFile,
                               RankFile rankFile, DopingFile dopingFile) {
        Athlete athlete = findOne(id);
        Rank rank = rankService.findOne(athleteDTO.getRankId());
        athlete.setRank(rank);
        School school = schoolService.findOne(athleteDTO.getSchoolId());
        athlete.setSchool(school);
        athlete.setStatus(athleteDTO.getStatus());
        athlete.setRegistryDate(athleteDTO.getRegistryDate());
        MedicalFile medical = medicalFileRepository.save(medicalFile);
        RankFile rankFile2 = rankFileRepository.save(rankFile);
        DopingFile doping = dopingFileRepository.save(dopingFile);
        athlete.setDopingFile(doping);
        athlete.setMedicalFile(medical);
        athlete.setRankFile(rankFile2);
        athlete.setStatus(Status.UNDER_CONSIDERATION.getName());
        repository.save(athlete);
        return AthleteDTO.from(athlete);
    }

    public AthleteDTO addCoach(int id, AthleteCoachAddDTO dto) {
        Athlete athlete = findOne(id);
        Coach coach = coachService.findOne(dto.getCoachId());
        athletesCoachesRepository.save(AthletesCoaches.builder()
                .athlete(athlete)
                .coach(coach)
                .school(athlete.getSchool())
                .role(dto.getRole())
                .registerDate(personService.dateFromString(dto.getRegisterDate()))
                .build());
        return AthleteDTO.from(athlete);
    }

    public List<Coach> coaches(Integer id) {
        Athlete athlete = findOne(id);
        List<AthletesCoaches> coachesAthletes = athletesCoachesRepository.findAllByAthleteIdAndSchoolId(id, athlete.getSchool().getId());
        List<Coach> coaches = new ArrayList<>();
        for (int i = 0; i < coachesAthletes.size(); i++) {
            if (coachesAthletes.get(i).getFinishDate() == null) {
                coaches.add(coachesAthletes.get(i).getCoach());
            }
        }
        return coaches;
    }

    public AthleteDTO deleteCoach(Integer id, Integer coachId) {
        Athlete athlete = findOne(id);
        List<AthletesCoaches> athletesCoaches = athletesCoachesRepository.findByAthleteIdAndCoachIdAndSchoolId(id, coachId, athlete.getSchool().getId());
        if (athletesCoaches.size() == 0){
            throw new ResourceNotFoundException("Запись о тренере и спортсмене", 0);
        }
        AthletesCoaches athCoach  = athletesCoaches.stream().filter(a -> a.getFinishDate() == null).findFirst().orElse(null);
        if (athCoach != null){
            athletesCoachesRepository.delete(athCoach);
        }
        return AthleteDTO.from(athlete);
    }

    public AthleteDTO finishCoach(Integer id, Integer coachId) {
        Athlete athlete = findOne(id);
        Coach coach = coachService.findOne(coachId);
        List<AthletesCoaches> athletesCoaches = athletesCoachesRepository.findByAthleteIdAndCoachIdAndSchoolId(id, coachId, athlete.getSchool().getId());
        if (athletesCoaches.size() == 0){
            throw new ResourceNotFoundException("Запись о тренере и спортсмене", 0);
        }
        AthletesCoaches athletesCoach = athletesCoaches.stream().filter(a -> a.getFinishDate() == null).findFirst().orElse(null);
        if (athletesCoach != null){
            athletesCoach.setFinishDate(LocalDate.now());
            athletesCoachesRepository.save(athletesCoach);
        }
        return AthleteDTO.from(athlete);
    }

    public List<Coach> universalCoaches(AthleteDTO athleteDTO) {
        List<AthletesCoaches> presentCoaches = presentCoaches(athleteDTO.getId());
        List<Coach> coaches = coachService.getByDisciplineAndSchool(athleteDTO);
        List<Coach> universalCoaches = new ArrayList<>(coaches);
        if (presentCoaches.size() > 0) {
            for (int j = 0; j < presentCoaches.size(); j++) {
                for (int i = 0; i < coaches.size(); i++) {
                    if (presentCoaches.get(j).getCoach().getId().equals(coaches.get(i).getId())) {
                        universalCoaches.remove(coaches.get(i));
                    }
                }
            }
        }
        return universalCoaches;
    }

    private boolean isTeam(String result) {
        return result.equals("да");
    }

    public List<AthletesCoaches> presentCoaches(Integer id){
        Athlete athlete = findOne(id);
        List<Coach> coaches = new ArrayList<>(new HashSet<>(athlete.getCoaches()));
        List<AthletesCoaches> athletesCoaches = athletesCoachesRepository.findAllByAthleteId(id);
        List<AthletesCoaches> presentCoaches = new ArrayList<>();
        for (int i = 0; i < coaches.size(); i++) {
            for (int j = 0; j < athletesCoaches.size(); j++) {
                if (coaches.get(i) == athletesCoaches.get(j).getCoach() && athletesCoaches.get(j).getFinishDate() == null){
                    presentCoaches.add(athletesCoaches.get(j));
                }
            }
        }
        return presentCoaches;
    }

    public Page<Athlete> search(SearchModel<AthleteFilter> searchModel){
        Specification<Athlete> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        return repository.findAll(specification, searchModel.getPagination().getPageRequest());
    }

    public List<CoachRole> roles(int id){
        List<AthletesCoaches> present = presentCoaches(id);
        List<CoachRole> roles = new ArrayList<>(List.of(CoachRole.values()));
        for (AthletesCoaches coach: present) {
            roles.removeIf(role -> role.getName().equals(coach.getRole()));
        }
        return roles;
    }

    public void save(Athlete athlete){
        repository.save(athlete);
    }

    public List<AthletesCoaches> history(int id){
        return athletesCoachesRepository.findAllByAthleteId(id);
    }
}
