package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.repository.AthletesTeamRepository;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AthletesTeamService {
    private final AthletesTeamRepository repository;
    private final DisciplineTypeService disciplineTypeService;
    private final CompetitionDisciplineAgeService agesService;
    private final ParticipationApplicationService participationApplicationService;

    public AthletesTeam findOne(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Команда", id);
        });
    }

    public List<AthletesTeam> all(){
        return repository.findAll();
    }

    public void save(int application, int number, int disciplineTypeId, int ageCategoryId){
        if (!existsByApplicationIdAndNumber(application, number)){
            DisciplineType disciplineType = disciplineTypeService.findOne(disciplineTypeId);
            ParticipationApplication participationApplication = participationApplicationService.findOne(application);
            AgeCategory ageCategory = agesService.findOne(ageCategoryId).getAgeCategory();
            repository.save(AthletesTeam.builder()
                    .application(participationApplication)
                    .competition(participationApplication.getCompetition())
                    .school(participationApplication.getSchool())
                    .disciplineType(disciplineType)
                    .ageCategory(ageCategory)
                    .number(number)
                    .build());
        }
    }

    public boolean existsByApplicationIdAndNumber(int id, int number){
        return repository.existsByApplicationIdAndNumber(id, number);
    }

    public void checkTeams(int application){
        List<ParticipationApplicationAthlete> applicationAthletes = participationApplicationService.findOne(application).getApplicationAthletes();
        for (int i = 0; i < applicationAthletes.size(); i++) {
            if (applicationAthletes.get(i).getTeamNumber() == null){
                applicationAthletes.remove(applicationAthletes.get(i));
            }
        }
        for (int i = 0; i < applicationAthletes.size(); i++) {
            List<AthletesTeam> athletesTeams = repository.findByApplicationIdAndNumber(application, applicationAthletes.get(i).getTeamNumber());
            for (int j = 1; j < athletesTeams.size(); j++) {
                repository.delete(athletesTeams.get(j));
            }
        }
    }
}
