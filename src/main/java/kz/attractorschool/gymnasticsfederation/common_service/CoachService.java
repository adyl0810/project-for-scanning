package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.DopingFile;
import kz.attractorschool.gymnasticsfederation.common_data.repository.AthletesCoachesRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.DopingFileRepository;
import kz.attractorschool.gymnasticsfederation.common_data.specification.CoachSpecification;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import kz.attractorschool.gymnasticsfederation.dto.*;
import kz.attractorschool.gymnasticsfederation.dto.add.CoachAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.search.CoachFilter;
import kz.attractorschool.gymnasticsfederation.dto.update.CoachUpdateDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.CoachCategoryFile;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CoachCategoryFileRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CoachRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@AllArgsConstructor
public class CoachService {
    private final CoachRepository coachRepository;
    private final PersonService personService;
    private final SchoolService schoolService;
    private final DisciplineService disciplineService;
    private final CoachCategoryService coachCategoryService;
    private final CoachCategoryFileRepository coachCategoryFileRepository;
    private final AthletesCoachesRepository athletesCoachesRepository;
    private final CoachSpecification specification;
    private final DopingFileRepository dopingFileRepository;

    public List<Coach> all() {
        return coachRepository.findAll();
    }

    public Coach findOne(Integer id) {
        return coachRepository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Тренер", id);
        });
    }

    public CoachDTO getOne(Integer id) {
        return CoachDTO.from(coachRepository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Тренер", id);
        }));
    }

    public List<Coach> allBySchoolId(Integer id) {
        return coachRepository.findAllBySchoolId(id);
    }

    public List<Coach> allBySchoolAndDisciplineId(Integer schoolId, Integer disciplineId) {
        return coachRepository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
    }

    public CoachDTO add(CoachAddDTO coachDTO, CoachCategoryFile coachCategoryFile, DopingFile dopingFile) {
        CoachCategoryFile categoryFile = coachCategoryFileRepository.save(coachCategoryFile);
        CoachCategory category = coachCategoryService.findOne(coachDTO.getCategoryId());
        School school = schoolService.findOne(coachDTO.getSchoolId());
        Discipline discipline = disciplineService.findOne(coachDTO.getDisciplineId());
        Person person = personService.findOne(coachDTO.getPersonId());
        DopingFile doping = dopingFileRepository.save(dopingFile);
        Coach coach = coachRepository.save(Coach.builder()
                .person(person)
                .school(school)
                .category(category)
                .categoryFile(categoryFile)
                .dopingFile(doping)
                .discipline(discipline)
                .build());
        String registryNum = "C-" + personService.addRegistryNumber(person.getGender(), discipline)
                + personService.countIdForRegistryNum(coach.getId());
        coach.setRegistryNumber(registryNum);
        coachRepository.save(coach);
        return CoachDTO.from(coach);
    }

    public String delete(Integer id) {
        Coach coach = findOne(id);
        coach.setDel(true);
        coachRepository.save(coach);
        return "ok";
    }

    public CoachDTO update(Integer id, CoachUpdateDTO coachUpdateDTO) {
        Coach coach = findOne(id);
        CoachCategory category = coachCategoryService.findOne(coachUpdateDTO.getCategoryId());
        coach.setCategory(category);
        Discipline discipline = disciplineService.findOne(coachUpdateDTO.getDisciplineId());
        coach.setDiscipline(discipline);
        coachRepository.save(coach);
        return CoachDTO.from(coach);
    }

    public Coach updateFile(Integer id, CoachCategoryFile categoryFile){
        Coach coach = findOne(id);
        CoachCategoryFile coachCategoryFile = coachCategoryFileRepository.save(categoryFile);
        coach.setCategoryFile(coachCategoryFile);
        return coach;
    }

    public Coach updateFile(Integer id, DopingFile dopingFile){
        Coach coach = findOne(id);
        coach.setDopingFile(dopingFileRepository.save(dopingFile));
        return coach;
    }

    public boolean isPdf(MultipartFile multipartFile) {
        String name = multipartFile.getOriginalFilename();
        String [] words = name.split("\\.");
        String format = words[words.length - 1];
        return format.equals("pdf");
    }

    public List<Coach> getByDisciplineAndSchool(AthleteDTO athleteDTO){
        return coachRepository.findAllBySchoolIdAndDisciplineId(athleteDTO.getSchool().getId(),
                athleteDTO.getDiscipline().getId());
    }

    public Page<Coach> search(SearchModel<CoachFilter> searchModel){
        Specification<Coach> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        return coachRepository.findAll(specification, searchModel.getPagination().getPageRequest());
    }

    public void save(Coach coach) {
        coachRepository.save(coach);
    }
}
