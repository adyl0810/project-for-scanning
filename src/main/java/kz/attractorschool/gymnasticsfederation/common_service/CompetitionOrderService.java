package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CompetitionOrderRepository;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CompetitionOrderService {
    private final CompetitionOrderRepository repository;
    private final ParticipationApplicationService applicationService;
    private final CompetitionService competitionService;

    public CompetitionOrder findOne(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Порядок", id);
        });
    }

    public HashMap<DisciplineType, List<HashMap<AgeCategory, HashMap<Integer, List<Athlete>>>>> athletesOrder(int id, int amountInFlow){
        List<ParticipationApplication> applications = applicationService.findAllByCompetitionId(id);
        List<ParticipationApplicationAthlete> applicationAthletes = new ArrayList<>();
        for (int i = 0; i < applications.size(); i++) {
            for (int j = 0; j < applications.get(i).getApplicationAthletes().size(); j++) {
                if (applications.get(i).getApplicationAthletes().get(j).getTeamNumber() <= 1){
                    applicationAthletes.add(applications.get(i).getApplicationAthletes().get(j));
                }
            }
        }
        List<DisciplineType> disciplineTypes =competitionService.findOne(id).getDisciplineTypes()
                .stream().filter(d -> d.getParticipantsAmountMax() == null || d.getParticipantsAmountMin() == null).collect(Collectors.toList());
        disciplineTypes = new ArrayList<>(new HashSet<>(disciplineTypes));
        List<AgeCategory> ageCategories = competitionService.findOne(id).getAgeCategories();
        HashMap<DisciplineType, List<AgeCategory>> byDisciplines = new HashMap<>();
        for (int i = 0; i < disciplineTypes.size(); i++) {
            List<AgeCategory> categories = new ArrayList<>(ageCategories);
            byDisciplines.put(disciplineTypes.get(i), categories);
        }

        HashMap<DisciplineType, List<HashMap<AgeCategory, List<Athlete>>>> byAgeAndDiscipline = new HashMap<>();
        for (int i = 0; i < disciplineTypes.size(); i++) {
            List<HashMap<AgeCategory, List<Athlete>>> byAgess = new ArrayList<>();
            for (int j = 0; j < ageCategories.size(); j++) {
                HashMap<AgeCategory, List<Athlete>> byAges = new HashMap<>();
                List<Athlete> athletess = new ArrayList<>();
                for (int k = 0; k < applicationAthletes.size(); k++) {
                    if (byDisciplines.get(disciplineTypes.get(i)).get(j).equals(applicationAthletes.get(k).getDisciplineAge().getAgeCategory())
                            && disciplineTypes.get(i).equals(applicationAthletes.get(k).getDisciplineType()) &&
                    applicationAthletes.get(k).getTeamNumber() <= 1){
                        athletess.add(applicationAthletes.get(k).getAthlete());
                    }
                    byAges.put(byDisciplines.get(disciplineTypes.get(i)).get(j), athletess);
                }
                byAgess.add(byAges);
            }
            byAgeAndDiscipline.put(disciplineTypes.get(i), byAgess);
        }

        HashMap<DisciplineType, List<HashMap<AgeCategory, HashMap<Integer, List<Athlete>>>>> orders = new HashMap<>();

        for (int i = 0; i < byAgeAndDiscipline.size(); i++) {
            List<HashMap<AgeCategory, HashMap<Integer, List<Athlete>>>> part = new ArrayList<>();
            for (int j = 0; j < ageCategories.size(); j++) {
                List <Athlete> athletes = byAgeAndDiscipline.get(disciplineTypes.get(i)).get(j).get(ageCategories.get(j));
                if(athletes == null){
                    return null;
                }
                int round = 1;
                HashMap<AgeCategory, HashMap<Integer, List<Athlete>>> p = new HashMap<>();
                HashMap <Integer, List<Athlete>> newMap = new HashMap<>();
                if (athletes.size() < amountInFlow) {
                    newMap.put(round, athletes);
                    p.put(ageCategories.get(j), newMap);
                    for (int k = 0; k < athletes.size(); k++) {
                        repository.save(saveOrder(athletes.get(k), id, disciplineTypes.get(i),
                                ageCategories.get(j), 1, k));
                    }
                }
                else {
                    int size = byAgeAndDiscipline.get(disciplineTypes.get(i)).get(j).get(ageCategories.get(j)).size();
                    int rounds = size / amountInFlow + 1;
                    for (int k = 0; k < rounds; k++) {
                        int number = 1;
                        int firstAthlete = amountInFlow * k;
                        int lastAthlete = firstAthlete + amountInFlow;
                        if (lastAthlete > size) {
                            lastAthlete = firstAthlete + (size % amountInFlow);
                        }
                        List<Athlete> athletess = new ArrayList<>();
                        for (int f = firstAthlete; f < lastAthlete; f++) {
                            athletess.add(athletes.get(f));
                            repository.save(saveOrder(athletes.get(f), id,
                                    disciplineTypes.get(i), ageCategories.get(j), k + 1, number));
                            number++;
                        }
                        newMap.put(round, athletess);
                        p.put(ageCategories.get(j), newMap);
                        round++;
                    }
                }
                part.add(p);
            }
            orders.put(disciplineTypes.get(i), part);
        }
        return orders;
    }

    public HashMap<DisciplineType, List<HashMap<AgeCategory, HashMap<Integer, List<AthletesTeam>>>>> teamsOrder(int id, int amountInFlow){
        List<ParticipationApplication> applications = applicationService.findAllByCompetitionId(id);
        List<AthletesTeam> teams = new ArrayList<>();
        for (int i = 0; i < applications.size(); i++) {
            teams.addAll(applications.get(i).getAthletesTeams());
        }
        List<DisciplineType> disciplineTypes = new ArrayList<>(new HashSet<>(competitionService.findOne(id).getDisciplineTypes()));
        List<AgeCategory> ageCategories = competitionService.findOne(id).getAgeCategories();
        HashMap<DisciplineType, List<AgeCategory>> byDisciplines = new HashMap<>();
        for (int i = 0; i < disciplineTypes.size(); i++) {
            List<AgeCategory> categories = new ArrayList<>(ageCategories);
            byDisciplines.put(disciplineTypes.get(i), categories);
        }

        HashMap<DisciplineType, List<HashMap<AgeCategory, List<AthletesTeam>>>> byAgeAndDiscipline = new HashMap<>();
        for (int i = 0; i < disciplineTypes.size(); i++) {
            List<HashMap<AgeCategory, List<AthletesTeam>>> byAgess = new ArrayList<>();
            for (int j = 0; j < ageCategories.size(); j++) {
                HashMap<AgeCategory, List<AthletesTeam>> byAges = new HashMap<>();
                List<AthletesTeam> teamss = new ArrayList<>();
                for (int k = 0; k < teams.size(); k++) {
                    if (byDisciplines.get(disciplineTypes.get(i)).get(j).equals(teams.get(k).getAgeCategory())
                            && disciplineTypes.get(i).equals(teams.get(k).getDisciplineType())){
                        teamss.add(teams.get(k));
                    }
                    byAges.put(byDisciplines.get(disciplineTypes.get(i)).get(j), teamss);
                }
                byAgess.add(byAges);
            }
            byAgeAndDiscipline.put(disciplineTypes.get(i), byAgess);
        }

        HashMap<DisciplineType, List<HashMap<AgeCategory, HashMap<Integer, List<AthletesTeam>>>>> orders = new HashMap<>();

        for (int i = 0; i < byAgeAndDiscipline.size(); i++) {
            List<HashMap<AgeCategory, HashMap<Integer, List<AthletesTeam>>>> part = new ArrayList<>();
            for (int j = 0; j < ageCategories.size(); j++) {
                List <AthletesTeam> athletesTeams = byAgeAndDiscipline.get(disciplineTypes.get(i)).get(j).get(ageCategories.get(j));
                if(athletesTeams == null){
                    return null;
                }
                int round = 1;
                HashMap<AgeCategory, HashMap<Integer, List<AthletesTeam>>> p = new HashMap<>();
                HashMap <Integer, List<AthletesTeam>> newMap = new HashMap<>();
                if (athletesTeams.size() < amountInFlow) {
                    newMap.put(round, athletesTeams);
                    p.put(ageCategories.get(j), newMap);
                    for (int k = 0; k < athletesTeams.size(); k++) {
                        repository.save(saveOrder(athletesTeams.get(k), id, disciplineTypes.get(i),
                                ageCategories.get(j), 1, k + 1));
                    }
                }
                else {
                    int size = byAgeAndDiscipline.get(disciplineTypes.get(i)).get(j).get(ageCategories.get(j)).size();
                    int rounds = size / amountInFlow + 1;
                    for (int k = 0; k < rounds; k++) {
                        int number = 1;
                        int firstAthlete = amountInFlow * k;
                        int lastAthlete = firstAthlete + amountInFlow;
                        if (lastAthlete > size) {
                            lastAthlete = firstAthlete + (size % amountInFlow);
                        }
                        List<AthletesTeam> athletess = new ArrayList<>();
                        for (int f = firstAthlete; f < lastAthlete; f++) {
                            athletess.add(athletesTeams.get(f));
                            repository.save(saveOrder(athletesTeams.get(f), id,
                                    disciplineTypes.get(i), ageCategories.get(j), k + 1, number));
                            number++;
                        }
                        newMap.put(round, athletess);
                        p.put(ageCategories.get(j), newMap);
                        round++;
                    }
                }
                part.add(p);
            }
            orders.put(disciplineTypes.get(i), part);
        }
        return orders;
    }

    private CompetitionOrder saveOrder(Athlete athlete, int competitionId, DisciplineType disciplineType,
                                      AgeCategory ageCategory, int flow, int number){
        CompetitionOrder order = CompetitionOrder.builder()
                .athlete(athlete)
                .competition(competitionService.findOne(competitionId))
                .discipline(disciplineType)
                .ageCategory(ageCategory)
                .flow(flow)
                .number(number)
                .build();
        return repository.save(order);
    }

    private CompetitionOrder saveOrder(AthletesTeam athletesTeam, int competitionId, DisciplineType disciplineType,
                                       AgeCategory ageCategory, int flow, int number){
        CompetitionOrder order = CompetitionOrder.builder()
                .athletesTeam(athletesTeam)
                .competition(competitionService.findOne(competitionId))
                .discipline(disciplineType)
                .ageCategory(ageCategory)
                .flow(flow)
                .number(number)
                .build();
        return repository.save(order);
    }

    public void deleteAllByCompetitionId(int id){
        List<CompetitionOrder> orders = repository.findAllByCompetitionId(id);
        repository.deleteAll(orders);
    }

    private List<CompetitionOrder> athleteByCompetitionAndFlow(int competitionId, int flow){
        return repository.findAllByCompetitionIdAndFlow(competitionId, flow).stream().filter(o -> o.getAthletesTeam() == null).collect(Collectors.toList());
    }

    private List<CompetitionOrder> teamByCompetitionAndFlow(int competitionId, int flow){
        return repository.findAllByCompetitionIdAndFlow(competitionId, flow).stream().filter(o -> o.getAthlete() == null).collect(Collectors.toList());
    }

    public List<CompetitionOrder> allByCompetition(int id){
        return repository.findAllByCompetitionIdOrderByFlow(id, Sort.by(Sort.Direction.ASC, "number"));
    }

    public boolean existsByCompetitionId(int id){
        return repository.existsByCompetitionId(id);
    }

    public void changeFlow(int competitionId, int id, int flow){
        CompetitionOrder order = findOne(id);
        int oldFlow = order.getFlow();
        List<CompetitionOrder> orders;
        List<CompetitionOrder> ordersNew;
        if (order.getAthletesTeam() == null){
            int lastNumber = athleteByCompetitionAndFlow(competitionId, flow).size() + 1;
            order.setFlow(flow);
            order.setNumber(lastNumber);
            repository.save(order);
            orders = athleteByCompetitionAndFlow(competitionId, oldFlow);
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).setNumber(i + 1);
            }
            ordersNew = athleteByCompetitionAndFlow(competitionId, flow);
            for (int i = 0; i < ordersNew.size(); i++) {
                ordersNew.get(i).setNumber(i + 1);
            }
        }
        else{
            int lastNumber = teamByCompetitionAndFlow(competitionId, flow).size() + 1;
            order.setFlow(flow);
            order.setNumber(lastNumber);
            repository.save(order);
            orders = teamByCompetitionAndFlow(competitionId, oldFlow);
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).setNumber(i + 1);
            }
            ordersNew = teamByCompetitionAndFlow(competitionId, flow);
            for (int i = 0; i < ordersNew.size(); i++) {
                ordersNew.get(i).setNumber(i + 1);
            }
        }
        repository.saveAll(orders);
        repository.saveAll(ordersNew);
    }

    public HashMap<DisciplineType, HashMap<AgeCategory, HashMap<Integer, List<CompetitionOrder>>>> teamsFlows(int competitionId){
        List<CompetitionOrder> orders = repository.findAllByCompetitionIdOrderByFlow(competitionId, Sort.by(Sort.Direction.ASC, "number")).stream().filter(o -> o.getAthlete() == null).collect(Collectors.toList());
        List<Integer> integers = new ArrayList<>(orders.stream().map(CompetitionOrder::getFlow).collect(Collectors.toSet()));
        HashMap<DisciplineType, HashMap<AgeCategory, HashMap<Integer, List<CompetitionOrder>>>> finish = new HashMap<>();
        HashMap<Integer, List<CompetitionOrder>> orderByFlow = new HashMap<>();
        List<AgeCategory> ageCategories = competitionService.findOne(competitionId).getAgeCategories();
        List<DisciplineType> disciplineTypes = new ArrayList<>(new HashSet<>(competitionService.findOne(competitionId).getDisciplineTypes()));

        for (int i = 0; i < disciplineTypes.size(); i++) {
            HashMap<AgeCategory, HashMap<Integer, List<CompetitionOrder>>> orderByAge = new HashMap<>();
            for (int j = 0; j < ageCategories.size(); j++) {
                for (int k = 0; k < integers.size(); k++) {
                    List<CompetitionOrder> competitionOrders = new ArrayList<>();
                    for (int d = 0; d < orders.size(); d++) {
                        if (integers.get(k) == orders.get(d).getFlow()
                                && orders.get(d).getAgeCategory().equals(ageCategories.get(j))
                                && orders.get(d).getDiscipline().equals(disciplineTypes.get(i))){
                            competitionOrders.add(orders.get(d));
                            orderByFlow.put(integers.get(k), competitionOrders);
                            orderByAge.put(ageCategories.get(j), orderByFlow);
                        }
                    }
                }
            }
            finish.put(disciplineTypes.get(i), orderByAge);
        }
        return finish;
    }

    public HashMap<DisciplineType, HashMap<AgeCategory, HashMap<Integer, List<CompetitionOrder>>>> athletesFlows(int competitionId){
        List<CompetitionOrder> orders = repository.findAllByCompetitionIdOrderByFlow(competitionId, Sort.by(Sort.Direction.ASC, "number")).stream().filter(o -> o.getAthletesTeam() == null).collect(Collectors.toList());
        List<Integer> integers = new ArrayList<>(orders.stream().map(CompetitionOrder::getFlow).collect(Collectors.toSet()));
        HashMap<DisciplineType, HashMap<AgeCategory, HashMap<Integer, List<CompetitionOrder>>>> finish = new HashMap<>();
        HashMap<Integer, List<CompetitionOrder>> orderByFlow = new HashMap<>();
        List<AgeCategory> ageCategories = competitionService.findOne(competitionId).getAgeCategories();
        List<DisciplineType> disciplineTypes = new ArrayList<>(new HashSet<>(competitionService.findOne(competitionId).getDisciplineTypes()));

        for (int i = 0; i < disciplineTypes.size(); i++) {
            HashMap<AgeCategory, HashMap<Integer, List<CompetitionOrder>>> orderByAge = new HashMap<>();
            for (int j = 0; j < ageCategories.size(); j++) {
                for (int k = 0; k < integers.size(); k++) {
                    List<CompetitionOrder> competitionOrders = new ArrayList<>();
                    for (int d = 0; d < orders.size(); d++) {
                        if (integers.get(k) == orders.get(d).getFlow()
                                && orders.get(d).getAgeCategory().equals(ageCategories.get(j))
                                && orders.get(d).getDiscipline().equals(disciplineTypes.get(i))){
                            competitionOrders.add(orders.get(d));
                            orderByFlow.put(integers.get(k), competitionOrders);
                            orderByAge.put(ageCategories.get(j), orderByFlow);
                        }
                    }
                }

            }
            finish.put(disciplineTypes.get(i), orderByAge);
        }
        return finish;
    }
}
