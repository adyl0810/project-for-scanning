package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScore;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CompetitionScoreRepository;
import kz.attractorschool.gymnasticsfederation.dto.add.CompetitionScoreAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.CompetitionScoreDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.CompetitionScoreUpdateDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CompetitionScoreService {
    private final CompetitionScoreRepository repository;
    private final ScoreCriterionService criterionService;
    private final CompetitionDisciplineService disciplineTypeService;
    private final CompetitionDisciplineProgramsService programService;
    private final CompetitionDisciplineAgeService ageService;
    private final ParticipationApplicationAthleteService applicationAthleteService;


    public List<CompetitionScore> all() {
        return repository.findAll();
    }

    public CompetitionScore findOne(int id) {
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Оценка", id);
        });
    }

    public CompetitionScore allByCriterionAndAthlete(CompetitionScoreAddDTO addDTO) {
        return repository.findByAgeCategoryIdAndAthleteIdAndProgramIdAndCriterionIdAndDisciplineTypeId(
                addDTO.getAgeCategoryId(),
                addDTO.getAthleteId(),
                addDTO.getProgramId(),
                addDTO.getCriterionId(),
                addDTO.getDisciplineTypeId());
    }

    public CompetitionScoreDTO getAll(int id) {
        return CompetitionScoreDTO.from(findOne(id));
    }

    public CompetitionScore add(CompetitionScoreAddDTO addDTO) {
        return repository.save(CompetitionScore.builder()
                .score(addDTO.getScore())
                .criterion(criterionService.findOne(addDTO.getCriterionId()))
                .disciplineType(disciplineTypeService.findOne(addDTO.getDisciplineTypeId()))
                .program(programService.findOne(addDTO.getProgramId()))
                .ageCategory(ageService.findOne(addDTO.getAgeCategoryId()))
                .athlete(applicationAthleteService.findOne(addDTO.getAthleteId()))
                .build());
    }

    public CompetitionScore update(CompetitionScoreUpdateDTO updateDTO, Integer id) {
        CompetitionScore score = repository.findById(id).orElseThrow();
        score.setScore(updateDTO.getScore());
        return repository.save(score);
    }

    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
