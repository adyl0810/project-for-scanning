package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScoreSum;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CompetitionScoreSumRepository;
import kz.attractorschool.gymnasticsfederation.dto.add.CompetitionScoreSumAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.CompetitionScoreSumGetDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CompetitionScoreSumService {

    private final CompetitionScoreSumRepository repository;
    private final CompetitionDisciplineService disciplineTypeService;
    private final CompetitionDisciplineProgramsService programService;
    private final CompetitionDisciplineAgeService ageService;
    private final ParticipationApplicationAthleteService applicationAthleteService;

    public List<CompetitionScoreSum> all() {
        return repository.findAll();
    }

    public CompetitionScoreSum allByAthleteAndDiscipline(CompetitionScoreSumGetDTO getDTO) {
        return repository.findByDisciplineTypeIdAndAgeCategoryIdAndProgramIdAndAthleteId(
                getDTO.getDisciplineTypeId(),
                getDTO.getAgeCategoryId(),
                getDTO.getProgramId(),
                getDTO.getAthleteId()
        );
    }

    public CompetitionScoreSum findOne(int id) {
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Оценка", id);
        });
    }

    public CompetitionScoreSum add(CompetitionScoreSumAddDTO addDTO) {
        return repository.save(CompetitionScoreSum.builder()
                .sum(addDTO.getSum())
                .disciplineType(disciplineTypeService.findOne(addDTO.getDisciplineTypeId()))
                .program(programService.findOne(addDTO.getProgramId()))
                .ageCategory(ageService.findOne(addDTO.getAgeCategoryId()))
                .athlete(applicationAthleteService.findOne(addDTO.getAthleteId()))
                .build());
    }
}
