package kz.attractorschool.gymnasticsfederation.common_service;


import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CompetitionFileRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.CompetitionRepository;
import kz.attractorschool.gymnasticsfederation.common_data.specification.ModelSpecification;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import kz.attractorschool.gymnasticsfederation.dto.*;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.CompetitionLevel;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.CompetitionStatus;
import kz.attractorschool.gymnasticsfederation.dto.add.CompetitionAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.search.CompetitionFilter;
import kz.attractorschool.gymnasticsfederation.dto.update.CompetitionUpdateDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.CompetitionPositionFile;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CompetitionService {
    private final CompetitionRepository competitionRepository;
    private final DisciplineService disciplineService;
    private final SchoolService schoolService;
    private final CompetitionFileRepository competitionFileRepository;
    private final ModelSpecification<Competition, CompetitionFilter> specification;
    private final SecretaryService secretaryService;

    public Competition findOne(int id) {
        return competitionRepository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Соревнование", id);
        });
    }

    public CompetitionDTO getOne(int id){
        return CompetitionDTO.from(findOne(id));
    }

    public List<Competition> all() {
        return competitionRepository.findAll();
    }

    public CompetitionDTO add(CompetitionAddDTO competitionAddDTO, CompetitionPositionFile positionFile) {
        Discipline discipline = disciplineService.findOne(competitionAddDTO.getDisciplineId());
        School school = schoolService.findOne(competitionAddDTO.getSchoolId());
        CompetitionPositionFile competitionPositionFile = competitionFileRepository.save(positionFile);
        Secretary secretary = secretaryService.findOne(competitionAddDTO.getSecretaryId());
        Competition competition = competitionRepository.save(Competition.builder()
                .name(competitionAddDTO.getName())
                .startDate(competitionAddDTO.getStartDate())
                .finishDate(competitionAddDTO.getFinishDate())
                .participationDate(competitionAddDTO.getParticipationDate())
                .country(competitionAddDTO.getCountry())
                .city(competitionAddDTO.getCity())
                .address(competitionAddDTO.getAddress())
                .areaName(competitionAddDTO.getAreaName())
                .level(competitionAddDTO.getLevel())
                .contact(competitionAddDTO.getContactName())
                .phone(competitionAddDTO.getContactPhone())
                .discipline(discipline)
                .secretary(secretary)
                .competitionPositionFile(competitionPositionFile)
                .school(school)
                .build());
        return CompetitionDTO.from(competition);
    }

    public CompetitionDTO update(Integer id, CompetitionUpdateDTO competitionUpdateDTO, CompetitionPositionFile positionFile) {
        Discipline discipline = disciplineService.findOne(competitionUpdateDTO.getDisciplineId());
        School school = schoolService.findOne(competitionUpdateDTO.getSchoolId());
        CompetitionPositionFile competitionPositionFile = competitionFileRepository.save(positionFile);
        Competition competition = findOne(id);
        competition.setName(competitionUpdateDTO.getName());
        competition.setStartDate(competitionUpdateDTO.getStartDate());
        competition.setFinishDate(competitionUpdateDTO.getFinishDate());
        competition.setParticipationDate(competitionUpdateDTO.getParticipationDate());
        competition.setLevel(competitionUpdateDTO.getLevel());
        competition.setCountry(competitionUpdateDTO.getCountry());
        competition.setCity(competitionUpdateDTO.getCity());
        competition.setAddress(competitionUpdateDTO.getAddress());
        competition.setAreaName(competitionUpdateDTO.getAreaName());
        competition.setContact(competitionUpdateDTO.getContactName());
        competition.setPhone(competitionUpdateDTO.getContactPhone());
        competition.setDiscipline(discipline);
        competition.setCompetitionPositionFile(competitionPositionFile);
        competition.setSchool(school);
        competitionRepository.save(competition);
        return CompetitionDTO.from(competition);
    }

    public String delete(Integer id){
        Competition competition = findOne(id);
        competition.setDel(true);
        competitionRepository.save(competition);
        return "ok";
    }

    public List<String> getLevels() {
        CompetitionLevel[] arr = CompetitionLevel.values();
        List<String> levels = new ArrayList<>();
        for (CompetitionLevel competitionLevel : arr) {
            levels.add(competitionLevel.getName());
        }
        return levels;
    }

    public boolean isPdf(MultipartFile multipartFile) {
        String name = multipartFile.getOriginalFilename();
        String[] words = name.split("\\.");
        String format = words[words.length - 1];
        return format.equals("pdf");
    }

    public CompetitionDTO confirm(int id){
        Competition competition = findOne(id);
        competition.setStatus(CompetitionStatus.CONFIRMED.getName());
        competitionRepository.save(competition);
        return CompetitionDTO.from(competition);
    }

    public Page<Competition> search(SearchModel<CompetitionFilter> searchModel){
        Specification<Competition> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        return competitionRepository.findAll(specification, searchModel.getPagination().getPageRequest());
    }
}
