package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.User;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.Role;
import kz.attractorschool.gymnasticsfederation.dto.FederationDTO;
import kz.attractorschool.gymnasticsfederation.dto.add.FederationAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.FederationUpdateDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Federation;
import kz.attractorschool.gymnasticsfederation.common_data.repository.FederationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FederationService {
    private final FederationRepository repository;
    private final UserService userService;

    public Federation findOne(Integer id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Федерация", id);
        });
    }

    public FederationDTO getOne(Integer id){
        return FederationDTO.from(repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Федерация", id);
        }));
    }

    public FederationDTO add(FederationAddDTO federationDTO){
        User user = userService.register(federationDTO.getEmail(), federationDTO.getPassword(), Role.valueOf("FEDERATION").name());
        Federation federation = repository.save(Federation.builder()
                .name(federationDTO.getName())
                .director(federationDTO.getDirector())
                .address(federationDTO.getAddress())
                .user(user)
                .phone(federationDTO.getPhone())
                .build());
        return FederationDTO.from(federation);
    }

    public String delete(Integer id){
        Federation federation = findOne(id);
        federation.setDel(true);
        repository.save(federation);
        return "ok";
    }

    public FederationDTO update(FederationUpdateDTO dto, Integer id){
        Federation federation = findOne(id);
        federation.setName(dto.getName());
        federation.setDirector(dto.getDirector());
        federation.setAddress(dto.getAddress());
        federation.setPhone(dto.getPhone());
        repository.save(federation);
        return FederationDTO.from(federation);
    }

    public Federation updateEmail(UserRegisterDTO dto, int id){
        Federation federation = findOne(id);
        User user = userService.changeUser(federation.getUser(), dto, "FEDERATION");
        federation.setUser(user);
        return repository.save(federation);
    }

    public void updatePassword(UserPasswordDTO dto, int id){
        Federation federation = findOne(id);
        userService.changePassword(federation.getUser(), dto);
    }

    public List<Federation> all(){
        return repository.findAll();
    }
}
