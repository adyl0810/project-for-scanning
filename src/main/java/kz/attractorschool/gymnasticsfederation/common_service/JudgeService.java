package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.repository.DopingFileRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.JudgeCategoryFileRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.JudgeRepository;
import kz.attractorschool.gymnasticsfederation.common_data.specification.JudgeSpecification;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import kz.attractorschool.gymnasticsfederation.dto.*;
import kz.attractorschool.gymnasticsfederation.dto.add.JudgeAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.search.JudgeFilter;
import kz.attractorschool.gymnasticsfederation.dto.update.JudgeUpdateDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@Service
@AllArgsConstructor
public class JudgeService {
    private final JudgeRepository repository;
    private final SchoolService schoolService;
    private final JudgeCategoryService categoryService;
    private final DisciplineService disciplineService;
    private final PersonService personService;
    private final JudgeCategoryFileRepository categoryFileRepository;
    private final JudgeSpecification specification;

    public List<Judge> all(){
        return repository.findAll();
    }

    public Judge findOne(Integer id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Судья", id);
        });
    }

    public List<Judge> allBySchoolId(Integer id) {
        return repository.findAllBySchoolId(id);
    }

    public List<Judge> allBySchoolAndDisciplineId(Integer schoolId, Integer disciplineId) {
        return repository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
    }

    public JudgeDTO getOne(Integer id){
        return JudgeDTO.from(repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Судья", id);
        }));
    }

    public JudgeDTO add(JudgeAddDTO judgeAddDTO, JudgeCategoryFile judgeCategoryFile){
        Person person = personService.findOne(judgeAddDTO.getPersonId());
        School school = schoolService.findOne(judgeAddDTO.getSchoolId());
        Discipline discipline = disciplineService.findOne(judgeAddDTO.getDisciplineId());
        JudgeCategory category = categoryService.findOne(judgeAddDTO.getCategoryId());
        JudgeCategoryFile categoryFile = categoryFileRepository.save(judgeCategoryFile);
        Judge judge = repository.save(Judge.builder()
                .person(person)
                .discipline(discipline)
                .category(category)
                .school(school)
                .categoryFile(categoryFile)
                .build());
        String registryNum = "J-" + personService.addRegistryNumber(person.getGender(), discipline)
                + personService.countIdForRegistryNum(judge.getId());
        judge.setRegistryNumber(registryNum);
        repository.save(judge);
        return JudgeDTO.from(judge);
    }

    public String delete(Integer id){
        Judge judge = findOne(id);
        judge.setDel(true);
        repository.save(judge);
        return "ok";
    }

    public JudgeDTO update(Integer id, JudgeUpdateDTO judgeUpdateDTO){
        Judge judge = findOne(id);
        Discipline discipline = disciplineService.findOne(judgeUpdateDTO.getDisciplineId());
        JudgeCategory category = categoryService.findOne(judgeUpdateDTO.getCategoryId());
        judge.setDiscipline(discipline);
        judge.setCategory(category);
        repository.save(judge);
        return JudgeDTO.from(judge);
    }

    public Judge updateFile(Integer id, JudgeCategoryFile categoryFile){
        Judge judge = findOne(id);
        judge.setCategoryFile(categoryFileRepository.save(categoryFile));
        return judge;
    }

    public boolean isPdf(MultipartFile multipartFile){
        String name = multipartFile.getOriginalFilename();
        String [] words = name.split("\\.");
        String format = words[words.length - 1];
        return format.equals("pdf");
    }

    public List<Judge> allBySchoolIdAndDisciplineId(int schoolId, int disciplineId){
        return repository.findAllBySchoolIdAndDisciplineId(schoolId, disciplineId);
    }

    public Page<Judge> search(SearchModel<JudgeFilter> searchModel){
        Specification<Judge> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        return repository.findAll(specification, searchModel.getPagination().getPageRequest());
    }

    public void save(Judge judge) {
        repository.save(judge);
    }
}
