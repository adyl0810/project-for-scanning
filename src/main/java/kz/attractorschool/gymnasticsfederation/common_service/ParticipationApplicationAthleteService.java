package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.dto.add.ParticipationApplicationAthleteAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.ParticipationApplicationAthleteDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.repository.ParticipationApplicationAthleteRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.ParticipationApplicationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ParticipationApplicationAthleteService {
    private final ParticipationApplicationAthleteRepository repository;
    private final ParticipationApplicationRepository applicationRepository;
    private final ParticipationApplicationService applicationService;
    private final AthleteService athleteService;
    private final CompetitionDisciplineAgeService agesService;
    private final DisciplineTypeService disciplineTypeService;
    private final DisciplineService disciplineService;
    private final AthletesTeamService athletesTeamService;

    public List<ParticipationApplicationAthlete> all(){
        return repository.findAll();
    }

    public ParticipationApplicationAthlete findOne(Integer id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Заявка", id);
        });
    }

    public ParticipationApplicationAthleteDTO getOne(Integer id){
        return ParticipationApplicationAthleteDTO.from(findOne(id));
    }

    public List<ParticipationApplicationAthlete> allByApplicationId(Integer id) {
        return repository.findAllByApplicationId(id);
    }

    public ParticipationApplicationAthlete add(ParticipationApplicationAthleteAddDTO dto) throws InterruptedException {
        ParticipationApplication application = applicationRepository.findById(dto.getApplicationId()).orElseThrow(() ->{
            return new ResourceNotFoundException("Заявка", 0);
        });

        // Проверка ниже по какой-то причине ломает пост запрос на странице подачи заявки на участие

//        if (repository.existsByApplicationIdAndAthleteIdAndDisciplineAgeIdAndDisciplineTypeId(dto.getApplicationId(), dto.getAthleteId(), dto.getDisciplineAgeId(), dto.getDisciplineTypeId())){
//            return repository.findByApplicationIdAndAthleteIdAndDisciplineAgeIdAndDisciplineTypeId(dto.getApplicationId(), dto.getAthleteId(), dto.getDisciplineAgeId(), dto.getDisciplineTypeId() ).orElseThrow(() -> {
//                return new ResourceNotFoundException("Заявка", 0);
//            });
//        }

//        if (!isSameDiscipline(dto.getAthleteId(), application.getCompetition().getDiscipline().getId())){
//            return null;
//        }

        DisciplineType disciplineType = disciplineTypeService.findOne(dto.getDisciplineTypeId());
        AgeCategory ageCategory = agesService.findOne(dto.getDisciplineAgeId()).getAgeCategory();
        ParticipationApplicationAthlete applicationAthlete = repository.save(
                ParticipationApplicationAthlete.builder()
                        .application(application)
                        .athlete(athleteService.findOne(dto.getAthleteId()))
                        .disciplineAge(agesService.findOne(dto.getDisciplineAgeId()))
                        .disciplineType(disciplineType)
                        .teamNumber(dto.getTeamNumber())
                        .build());
        if (dto.getTeamNumber() > 0){
            athletesTeamService.save(dto.getApplicationId(), dto.getTeamNumber(), dto.getDisciplineTypeId(), dto.getDisciplineAgeId());
        }
        return applicationAthlete;
    }

    public void delete(int applicationId, int id){
        ParticipationApplicationAthlete applicationAthlete = repository.findByIdAndApplicationId(applicationId, id).orElseThrow(() -> {
            return new ResourceNotFoundException("Заявка", id);
        });
        repository.delete(applicationAthlete);
    }

    public void delete(List<ParticipationApplicationAthlete> applicationAthletes){
        repository.deleteAll(applicationAthletes);
    }

    private boolean isSameDiscipline(int athleteId, int disciplineId){
        return athleteService.getOne(athleteId).getDiscipline().getId() == disciplineId;
    }

    private boolean isCorrectAge(int maxAge, int minAge, int athleteId){
        int year = athleteService.getOne(athleteId).getPerson().getBirthdayDate().getYear();
        return year <= maxAge && year >= minAge;
    }

    public boolean isCorrectAge(int maxAge, int athleteId){
        int year = athleteService.getOne(athleteId).getPerson().getBirthdayDate().getYear();
        return year <= maxAge;
    }

//    private List<AthletesTeam> teams(int applicationId){
//        List<ParticipationApplicationAthlete> all = repository.findAllByApplicationId(applicationId);
//        ParticipationApplication application = applicationService.findOne(applicationId);
//        all = all.stream().filter(a -> a.getTeamNumber() != null).distinct().collect(Collectors.toList());
//        for (int i = 0; i < all.size(); i++) {
//            athletesTeamService.save(application, all.get(i).getTeamNumber(), all.get(i).getDisciplineType(),
//                    all.get(i).getDisciplineAge().getAgeCategory());
//        }
//        return
//    }

//    private boolean isCorrectAge(int minAge, int athleteId){
//        int year = athleteService.getOne(athleteId).getPerson().getBirthday().getYear();
//        return year >= minAge;
//    }
}
