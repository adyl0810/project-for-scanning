package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.repository.ParticipationApplicationAthleteRepository;
import kz.attractorschool.gymnasticsfederation.dto.ParticipationApplicationDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.repository.ParticipationApplicationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ParticipationApplicationService {
    private final ParticipationApplicationRepository repository;
    private final SchoolService schoolService;
    private final CompetitionService competitionService;
    private final ParticipationApplicationAthleteRepository applicationAthleteService;
    private final ParticipationApplicationCoachService applicationCoachService;
    private final ParticipationApplicationJudgeService applicationJudgeService;

    public List<ParticipationApplication> all(){
        return repository.findAll();
    }

    public ParticipationApplication findOne(Integer id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Заявка", id);
        });
    }

    public ParticipationApplicationDTO getOne(Integer id){
        return ParticipationApplicationDTO.from(findOne(id));
    }

    public List<ParticipationApplication> allByCompetitionId(Integer id) {
        return repository.findAllByCompetitionId(id);
    }

    public ParticipationApplicationDTO add(int schoolId, int competitionId){
        if (repository.existsBySchoolIdAndCompetitionId(schoolId, competitionId)){
            return ParticipationApplicationDTO.from(repository.findBySchoolIdAndCompetitionId(schoolId, competitionId).orElseThrow(() -> {
                return new ResourceNotFoundException("Заявка", 0);
            }));
        }
        ParticipationApplication newApplication = repository.save(
                ParticipationApplication.builder()
                        .competition(competitionService.findOne(competitionId))
                        .school(schoolService.findOne(schoolId))
                        .build());
        return ParticipationApplicationDTO.from(newApplication);
    }


    public void delete(int id){
        ParticipationApplication application = findOne(id);
        applicationAthleteService.deleteAll(application.getApplicationAthletes());
        applicationCoachService.delete(application.getApplicationCoaches());
        applicationJudgeService.delete(application.getApplicationJudges());
        repository.delete(findOne(id));
    }

//    public HashMap<AgeCategory, HashMap<Integer, List<Athlete>>> athletesOrder(int id){
//        List<ParticipationApplication> applications = repository.findAllByCompetitionId(id);
//        List<ParticipationApplicationAthlete> applicationAthletes = new ArrayList<>();
//        for (int i = 0; i < applications.size(); i++) {
//            applicationAthletes.addAll(applications.get(i).getApplicationAthletes());
//        }
//        List<AgeCategory> ageCategories = competitionService.findOne(id).getAgeCategories();
//        List<DisciplineType> disciplineTypes = competitionService.findOne(id).getDisciplineTypes();
//        int ages = ageCategories.size();
//        HashMap<AgeCategory, List<Athlete>> byAge = new HashMap<>();
//        for (int i = 0; i < ages; i++) {
//            byAge.put(ageCategories.get(i), new ArrayList<>());
//        }
//        for (int i = 0; i < applicationAthletes.size(); i++) {
//            for (int j = 0; j < ageCategories.size(); j++) {
//                AgeCategory ageCategory = applicationAthletes.get(i).getDisciplineAge().getAgeCategory();
//                if (ageCategories.get(j).equals(ageCategory)){
//                    byAge.get(ageCategory).add(applicationAthletes.get(i).getAthlete());
//                }
//            }
//        }
//
//        HashMap<AgeCategory, HashMap<Integer, List<Athlete>>> orders = new HashMap<>();
//        int round = 1;
//        for (int i = 0; i < ages; i++) {
//            HashMap <Integer, List<Athlete>> newMap = new HashMap<>();
//            if (byAge.get(ageCategories.get(i)).size() < 10){
//                newMap.put(round, byAge.get(ageCategories.get(i)));
//                orders.put(ageCategories.get(i), newMap);
//            }else{
//                int rounds = byAge.get(ageCategories.get(i)).size() / 10 + 1;
//                for (int k = 0; k < rounds; k++) {
//                    List<Athlete> athletes = new ArrayList<>();
//                    int firstAthlete = 10 * k;
//                    int lastAthlete = firstAthlete + 10;
//                    if (lastAthlete > byAge.get(ageCategories.get(i)).size()){
//                        lastAthlete = firstAthlete + (byAge.get(ageCategories.get(i)).size() % 10);
//                    }
//                    for (int j = firstAthlete; j < lastAthlete; j++) {
//                        athletes.add(byAge.get(ageCategories.get(i)).get(j));
//                    }
//                    newMap.put(round, athletes);
//                    orders.put(ageCategories.get(i), newMap);
//                    round++;
//                }
//            }
//        }
//        return orders;
//    }

    public List<ParticipationApplication> findAllByCompetitionId(int id) {
        return repository.findAllByCompetitionId(id);
    }

    public boolean exist(int id){
        return repository.existsByCompetitionId(id);
    }
}
