package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Discipline;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Gender;

import kz.attractorschool.gymnasticsfederation.common_data.repository.GenderRepository;
import kz.attractorschool.gymnasticsfederation.dto.PersonDTO;
import kz.attractorschool.gymnasticsfederation.dto.search.PersonFilter;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.PersonPhoto;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Person;
import kz.attractorschool.gymnasticsfederation.common_data.repository.PersonPhotoRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.PersonRepository;
import kz.attractorschool.gymnasticsfederation.common_data.specification.ModelSpecification;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository repository;
    private final PersonPhotoRepository personPhotoRepository;
    private final ModelSpecification<Person, PersonFilter> specification;
    private final GenderRepository genderRepository;

    public Person findOne(Integer id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Персона", id);
        });
    }

    public PersonDTO getOne(Integer id){
        return PersonDTO.from(findOne(id));
    }

    public PersonDTO add(PersonPhoto personPhoto, PersonDTO personDTO){
        PersonPhoto photo = personPhotoRepository.save(personPhoto);
        Person person = repository.save(Person.builder()
                .surname(personDTO.getSurname())
                .name(personDTO.getName())
                .middleName(personDTO.getMiddleName())
                .photo(photo)
                .birthday(dateFromString(personDTO.getBirthday()))
                .iin(personDTO.getIin())
                .gender(genderRepository.findByName(personDTO.getGender()))
                .city(personDTO.getCity())
                .address(personDTO.getAddress())
                .phone(personDTO.getPhone())
                .email(personDTO.getEmail())
                .education(personDTO.getEducation())
                .comment(personDTO.getComment())
                .build());
        return PersonDTO.from(person);
    }

    public String delete(Integer id){
        Person person = findOne(id);
        person.setDel(true);
        repository.save(person);
        return "ok";
    }

    public PersonDTO update(PersonDTO personDTO, Integer id){
        Person person = findOne(id);
        person.setName(personDTO.getName());
        if(!person.getSurname().equals(personDTO.getSurname())){
            person.setOldSurname(personDTO.getSurname());
            person.setSurname(personDTO.getSurname());
        }
        person.setMiddleName(personDTO.getMiddleName());
        person.setAddress(personDTO.getAddress());
        person.setBirthday(dateFromString(personDTO.getBirthday()));
        person.setIin(personDTO.getIin());
        person.setCity(personDTO.getCity());
        person.setComment(personDTO.getComment());
        person.setEducation(personDTO.getEducation());
        person.setEmail(personDTO.getEmail());
        person.setGender(genderRepository.findByName(personDTO.getGender()));
        person.setPhone(personDTO.getPhone());
        repository.save(person);
        return PersonDTO.from(person);
    }

    public PersonDTO updatePhoto(PersonPhoto personPhoto, Integer id){
        Person person = findOne(id);
        PersonPhoto photo = personPhotoRepository.save(personPhoto);
        person.setPhoto(photo);
        repository.save(person);
        return PersonDTO.from(person);
    }

    public List<Person> all(){
        return repository.findAll();
    }

    public boolean isUnique(String iin){
        return repository.existsByIin(iin);
    }

    public Page<Person> search(SearchModel<PersonFilter> searchModel){
        Specification<Person> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        return repository.findAll(specification, searchModel.getPagination().getPageRequest());
    }

    public String addRegistryNumber(Gender gender, Discipline discipline) {
        String number = "0" + gender.getId() + "-";
        number += "0" + discipline.getId() + "-";
        return number;
    }

    public String countIdForRegistryNum(int id){
        if (id < 10){
            return "00000" + id;
        }
        else if(id > 10 && id < 100){
            return "0000" + id;
        }
        else if(id > 100 && id < 1000){
            return "000" + id;
        }
        else if(id > 1000 && id < 10000){
            return "00" + id;
        }
        else {
            return "0" + id;
        }
    }

    public LocalDate dateFromString(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }

    public boolean isPhoto(MultipartFile multipartFile) {
        String name = multipartFile.getOriginalFilename();
        String[] words = name.split("\\.");
        String format = words[words.length - 1];
        return format.equals("png") || format.equals("jpg");
    }
}
