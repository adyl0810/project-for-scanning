package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.SchoolChangeStatus;
import kz.attractorschool.gymnasticsfederation.common_data.repository.AthletesCoachesRepository;
import kz.attractorschool.gymnasticsfederation.common_data.repository.SchoolChangeRepository;
import kz.attractorschool.gymnasticsfederation.dto.add.SchoolChangeAddDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SchoolChangeService {
    private final SchoolChangeRepository repository;
    private final SchoolService schoolService;
    private final AthleteService athleteService;
    private final CoachService coachService;
    private final JudgeService judgeService;
    private final AthletesCoachesRepository athletesCoachesRepository;

    public List<SchoolChange> all(){
        return repository.findAll();
    }

    public SchoolChange findOne(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Заявка на смену школы", id);
        });
    }

    public SchoolChange add(SchoolChangeAddDTO dto, Athlete athlete){
        School oldSchool = athlete.getSchool();
        return repository.save(SchoolChange.builder()
                        .athlete(athlete)
                        .oldSchool(oldSchool)
                        .newSchool(schoolService.findOne(dto.getNewSchool()))
                        .build());
    }

    public SchoolChange add(SchoolChangeAddDTO dto, Coach coach){
        School oldSchool = coach.getSchool();
        return repository.save(SchoolChange.builder()
                .coach(coach)
                .oldSchool(oldSchool)
                .newSchool(schoolService.findOne(dto.getNewSchool()))
                .build());
    }

    public SchoolChange add(SchoolChangeAddDTO dto, Judge judge){
        School oldSchool = judge.getSchool();
        return repository.save(SchoolChange.builder()
                .judge(judge)
                .oldSchool(oldSchool)
                .newSchool(schoolService.findOne(dto.getNewSchool()))
                .build());
    }

    public SchoolChange confirm(int id){
        SchoolChange schoolChange = findOne(id);
        schoolChange.setStatus(SchoolChangeStatus.CONFIRMED.getName());
        schoolChange.setDate(LocalDate.now());
        if (schoolChange.getAthlete() != null){
            Athlete athlete = athleteService.findOne(schoolChange.getAthlete().getId());
            athlete.setSchool(schoolChange.getNewSchool());
            athleteService.save(athlete);
            List<AthletesCoaches> athletesCoaches = athletesCoachesRepository.findAllByAthleteIdAndSchoolId(athlete.getId(), schoolChange.getOldSchool().getId());
            checkAthletesAndCoaches(athletesCoaches);
        }
        else if (schoolChange.getCoach() != null){
            Coach coach = coachService.findOne(schoolChange.getCoach().getId());
            coach.setSchool(schoolChange.getNewSchool());
            coachService.save(coach);
            List<AthletesCoaches> athletesCoaches = athletesCoachesRepository.findAllByCoachIdAndSchoolId(coach.getId(), schoolChange.getOldSchool().getId());
            checkAthletesAndCoaches(athletesCoaches);
        }
        else{
            Judge judge = judgeService.findOne(schoolChange.getJudge().getId());
            judge.setSchool(schoolChange.getNewSchool());
            judgeService.save(judge);
        }
        return repository.save(schoolChange);
    }

    public SchoolChange cancel(int id){
        SchoolChange schoolChange = findOne(id);
        schoolChange.setStatus(SchoolChangeStatus.CANCELED.getName());
        schoolChange.setDate(LocalDate.now());
        return repository.save(schoolChange);
    }

    private void checkAthletesAndCoaches(List<AthletesCoaches> athletesCoaches){
        for (int i = 0; i < athletesCoaches.size(); i++) {
            if (!athletesCoaches.get(i).isDel() && athletesCoaches.get(i).getFinishDate() == null){
                athletesCoaches.get(i).setFinishDate(LocalDate.now());
                athletesCoachesRepository.save(athletesCoaches.get(i));
            }
        }
    }
}
