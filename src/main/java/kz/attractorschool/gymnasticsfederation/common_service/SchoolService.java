package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.User;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.Role;
import kz.attractorschool.gymnasticsfederation.dto.SchoolDTO;
import kz.attractorschool.gymnasticsfederation.dto.add.SchoolAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.SchoolUpdateDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Federation;
import kz.attractorschool.gymnasticsfederation.common_data.entity.School;
import kz.attractorschool.gymnasticsfederation.common_data.repository.SchoolRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SchoolService {
    private final SchoolRepository repository;
    private final FederationService federationService;
    private final UserService userService;

    public School findOne(Integer id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Школа", id);
        });
    }

    public SchoolDTO getOne(Integer id){
        return SchoolDTO.from(repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Школа", id);
        }));
    }

    public SchoolDTO add(SchoolAddDTO schoolDTO){
        Federation federation = federationService.findOne(schoolDTO.getFederationId());
        User user = userService.register(schoolDTO.getEmail(), schoolDTO.getPassword(), Role.valueOf("SCHOOL").name());
                School school = repository.save(School.builder()
                .name(schoolDTO.getName())
                .address(schoolDTO.getAddress())
                .director(schoolDTO.getDirector())
                .user(user)
                .phone(schoolDTO.getPhone())
                .federation(federation)
                .build());
        return SchoolDTO.from(school);
    }

    public void delete(Integer id){
        School school = findOne(id);
        school.setDel(true);
        repository.save(school);
    }

    public SchoolDTO update(SchoolUpdateDTO schoolDTO, Integer id){
        School school = findOne(id);
        school.setName(schoolDTO.getName());
        school.setAddress(schoolDTO.getAddress());
        school.setDirector(schoolDTO.getDirector());
        school.setPhone(schoolDTO.getPhone());
        repository.save(school);
        return SchoolDTO.from(school);
    }

    public School updateEmail(UserRegisterDTO dto, int id){
        School school = findOne(id);
        User user = userService.changeUser(school.getUser(), dto, "SCHOOL");
        school.setUser(user);
        return repository.save(school);
    }

    public void updatePassword(UserPasswordDTO dto, int id){
        School school = findOne(id);
        userService.changePassword(school.getUser(), dto);
    }

    public List<School> all(){
        return repository.findAll();
    }

    public School findByUser(User user){
        return repository.findByUser(user).orElseThrow(() -> {
            return new ResourceNotFoundException("Школа с мейлом", user.getEmail());
        });
    }
}
