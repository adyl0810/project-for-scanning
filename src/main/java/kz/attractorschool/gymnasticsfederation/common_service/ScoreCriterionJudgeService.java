package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterionJudge;
import kz.attractorschool.gymnasticsfederation.common_data.repository.ScoreCriterionJudgeRepository;
import kz.attractorschool.gymnasticsfederation.dto.add.ScoreCriterionJudgeAddDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ScoreCriterionJudgeService {

    private final ScoreCriterionJudgeRepository repository;
    private final ParticipationApplicationJudgeService judgeService;
    private final ScoreCriterionService criterionService;

    public List<ScoreCriterionJudge> all() {
        return repository.findAll();
    }

    public List<ScoreCriterionJudge> allByCompetitionId(Integer id) {
        return repository.findAllByJudgeApplicationCompetitionIdOrderByCriterionName(id);
    }

    public ScoreCriterionJudge add(ScoreCriterionJudgeAddDTO addDTO) {
        return repository.save(ScoreCriterionJudge.builder()
                .judge(judgeService.findOne(addDTO.getJudgeId()))
                .criterion(criterionService.findOne(addDTO.getCriterionId()))
                .build());
    }
}
