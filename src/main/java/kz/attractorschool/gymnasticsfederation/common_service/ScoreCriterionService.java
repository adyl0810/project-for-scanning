package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterion;
import kz.attractorschool.gymnasticsfederation.common_data.repository.ScoreCriterionRepository;
import kz.attractorschool.gymnasticsfederation.dto.add.ScoreCriterionAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.ScoreCriterionDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ScoreCriterionService {
    private final ScoreCriterionRepository repository;
    private final DisciplineService disciplineService;
    private final DisciplineTypeService disciplineTypeService;

    public List<ScoreCriterion> all(){
        return repository.findAll();
    }

    public ScoreCriterion findOne(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Критерий", id);
        });
    }

    public List<ScoreCriterion> allByDisciplineId(Integer id) {
        return repository.findAllByDisciplineId(id);
    }

    public ScoreCriterionDTO getOne(int id){
        return ScoreCriterionDTO.from(findOne(id));
    }

    public ScoreCriterion add(ScoreCriterionAddDTO dto) {
        ScoreCriterion criterion = repository.save(ScoreCriterion
                        .builder()
                        .name(dto.getName())
                        .discipline(disciplineService.findOne(dto.getDisciplineId()))
                        .build());
        if (dto.getMinScore() != null){
            criterion.setMinScore(dto.getMinScore());
        }
        else if (dto.getMaxScore() != null){
            criterion.setMaxScore(dto.getMaxScore());
        }
        else if(dto.getDisciplineTypeId() > 0){
            criterion.setDisciplineType(disciplineTypeService.findOne(dto.getDisciplineTypeId()));
        }
        return repository.save(criterion);
    }

    public ScoreCriterion update(int id, ScoreCriterionAddDTO dto){
        ScoreCriterion criterion = findOne(id);
        criterion.setName(dto.getName());
        criterion.setDiscipline(disciplineService.findOne(dto.getDisciplineId()));
        if (dto.getMinScore() != null){
            criterion.setMinScore(dto.getMinScore());
        }
        if (dto.getMaxScore() != null){
            criterion.setMaxScore(dto.getMaxScore());
        }
        return repository.save(criterion);
    }

    public void delete(int id){
        ScoreCriterion criterion = findOne(id);
        criterion.setDel(true);
        repository.save(criterion);
    }
}
