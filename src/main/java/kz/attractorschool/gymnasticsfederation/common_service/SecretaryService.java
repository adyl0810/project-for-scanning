package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.School;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Secretary;
import kz.attractorschool.gymnasticsfederation.common_data.entity.User;
import kz.attractorschool.gymnasticsfederation.common_data.enumiration.Role;
import kz.attractorschool.gymnasticsfederation.common_data.repository.SecretaryRepository;
import kz.attractorschool.gymnasticsfederation.dto.add.SecretaryAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.SecretaryUpdateDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SecretaryService {
    private final SecretaryRepository repository;
    private final UserService userService;
    private final SchoolService schoolService;

    public Secretary findOne(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Секретарь", id);
        });
    }

    public List<Secretary> all(){
        return repository.findAll();
    }

    public Secretary add(SecretaryAddDTO dto) {
        School school = schoolService.findOne(dto.getSchoolId());
        User user = userService.register(dto.getEmail(), dto.getPassword(), Role.valueOf("SECRETARY").name());
        Secretary secretary = Secretary.builder()
                .name(dto.getName())
                .surname(dto.getSurname())
                .school(school)
                .user(user)
                .build();
        return repository.save(secretary);
    }

    public Secretary update(SecretaryUpdateDTO dto, int id) {
        Secretary secretary = findOne(id);
        secretary.setName(dto.getName());
        secretary.setSurname(dto.getSurname());
        return repository.save(secretary);
    }

    public void delete(int id){
        Secretary secretary = findOne(id);
        secretary.setDel(true);
        repository.save(secretary);
    }

    public Secretary updateEmail(UserRegisterDTO dto, int id) {
        Secretary secretary = findOne(id);
        User user = userService.changeUser(secretary.getUser(), dto, "SECRETARY");
        secretary.setUser(user);
        return repository.save(secretary);
    }

    public void updatePassword(UserPasswordDTO dto, Integer id) {
        Secretary secretary = findOne(id);
        userService.changePassword(secretary.getUser(), dto);
    }

    public List<Secretary> findBySchool(int schoolId){
        return repository.findBySchoolId(schoolId);
    }
}
