package kz.attractorschool.gymnasticsfederation.common_service;

import kz.attractorschool.gymnasticsfederation.common_data.entity.User;
import kz.attractorschool.gymnasticsfederation.common_data.repository.UserRepository;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.exception.UserAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository repository;
    private final PasswordEncoder encoder;

    public User register(String email, String password, String role){
        if(repository.existsByEmail(email))
            throw new UserAlreadyExistsException();
        User user = User.builder()
                .email(email)
                .password(encoder.encode(password))
                .role(role)
                .build();
        return repository.save(user);
    }

    public User findByEmail(String email){
        return repository.findByEmail(email).orElseThrow(() -> {
            return new ResourceNotFoundException("Пользователь", email);
        });
    }

    public User changeUser(User user, UserRegisterDTO dto, String role){
        User newUser = user;
        boolean nn = encoder.matches(dto.getOldPassword(), user.getPassword());
        if (user.getEmail().equals(dto.getOldEmail()) && encoder.matches(dto.getOldPassword(), user.getPassword())){
            user.setActive(false);
            newUser = register(dto.getEmail(), dto.getPassword(), role);
            repository.save(newUser);
            repository.save(user);
        }
        return newUser;
    }

    public User changePassword(User user, UserPasswordDTO dto){
        if (encoder.matches(dto.getOldPassword(), user.getPassword())){
            user.setPassword(encoder.encode(dto.getPassword()));
        }
        return repository.save(user);
    }
}
