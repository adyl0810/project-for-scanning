package kz.attractorschool.gymnasticsfederation.common_web.controller.api;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScore;
import kz.attractorschool.gymnasticsfederation.common_service.CompetitionScoreService;
import kz.attractorschool.gymnasticsfederation.dto.add.CompetitionScoreAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.CompetitionScoreUpdateDTO;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/api/score")
public class CompetitionScoreRestController {

    private final CompetitionScoreService service;

    @GetMapping
    public CompetitionScore get(@Valid CompetitionScoreAddDTO addDTO) {
        return service.allByCriterionAndAthlete(addDTO);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SECRETARY')")
    public Integer add(@Valid CompetitionScoreAddDTO addDTO) {
        return service.add(addDTO).getId();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SECRETARY')")
    public void update(@Valid CompetitionScoreUpdateDTO updateDTO,
                       @PathVariable Integer id) {
        service.update(updateDTO, id);
    }
}
