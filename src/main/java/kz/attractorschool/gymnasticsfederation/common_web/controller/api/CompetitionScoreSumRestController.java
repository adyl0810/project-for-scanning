package kz.attractorschool.gymnasticsfederation.common_web.controller.api;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScoreSum;
import kz.attractorschool.gymnasticsfederation.common_service.CompetitionScoreSumService;
import kz.attractorschool.gymnasticsfederation.dto.add.CompetitionScoreSumAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.CompetitionScoreSumGetDTO;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/api/score/sum")
public class CompetitionScoreSumRestController {

    private final CompetitionScoreSumService service;

    @GetMapping
    public CompetitionScoreSum get(@Valid CompetitionScoreSumGetDTO getDTO) {
        return service.allByAthleteAndDiscipline(getDTO);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SECRETARY')")
    public void add(@Valid CompetitionScoreSumAddDTO addDTO) {
        service.add(addDTO);
    }
}
