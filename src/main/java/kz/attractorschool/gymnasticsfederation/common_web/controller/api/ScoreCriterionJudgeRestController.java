package kz.attractorschool.gymnasticsfederation.common_web.controller.api;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterionJudge;
import kz.attractorschool.gymnasticsfederation.common_service.ScoreCriterionJudgeService;
import kz.attractorschool.gymnasticsfederation.dto.add.ScoreCriterionJudgeAddDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/score/judge")
@AllArgsConstructor
public class ScoreCriterionJudgeRestController {

    private final ScoreCriterionJudgeService service;

    @PostMapping
    public ScoreCriterionJudge add(@Valid ScoreCriterionJudgeAddDTO addDTO) {
        return service.add(addDTO);
    }
}
