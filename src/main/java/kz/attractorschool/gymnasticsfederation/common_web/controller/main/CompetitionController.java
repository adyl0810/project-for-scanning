package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Competition;
import kz.attractorschool.gymnasticsfederation.common_data.entity.School;
import kz.attractorschool.gymnasticsfederation.common_data.specification.PaginationModel;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SortingModel;
import kz.attractorschool.gymnasticsfederation.dto.search.CompetitionFilter;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_service.*;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashSet;

@Controller
@AllArgsConstructor
@RequestMapping("/competitions")
public class CompetitionController {
    private final CompetitionService service;
    private final DisciplineService disciplineService;
    private final CompetitionDisciplineAgeService competitionDisciplineAgeService;
    private final CompetitionDisciplineProgramsService competitionDisciplineProgramsService;
    private final DisciplineTypeService disciplineTypeService;
    private final StorageService storageService;
    private final CompetitionProgramService competitionProgramService;
    private final RankService rankService;
    private final SchoolService schoolService;
    private final AgeCategoryService ageCategoryService;
    private final ParticipationApplicationService applicationService;
    private final CompetitionOrderService competitionOrderService;
    private final SecretaryService secretaryService;
    private final UserService userService;

    @GetMapping
    public String all(Model model){
        model.addAttribute("competitions", service.all());
        model.addAttribute("disciplines", disciplineService.all());
        return "competition/competitions";
    }

    @GetMapping("/{id}")
    public String getComp(@PathVariable Integer id,
                          Model model) {
        model.addAttribute("competition", service.findOne(id));
        model.addAttribute("disciplines", new HashSet<>(service.findOne(id).getDisciplineTypes()));
        model.addAttribute("competitionAges", competitionDisciplineAgeService.findByCompetitionId(id));
        model.addAttribute("competitionPrograms", competitionDisciplineProgramsService.findByCompetitionId(id));
        return "competition/competition";
    }

    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    @GetMapping("/add")
    public String getCompForm(Model model, Principal principal) {
        School school = schoolService.findByUser(userService.findByEmail(principal.getName()));
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("disciplineTypes", disciplineTypeService.all());
        model.addAttribute("competitionPrograms", competitionProgramService.all());
        model.addAttribute("ranks", rankService.all());
        model.addAttribute("school", school);
        model.addAttribute("ageCategories", ageCategoryService.all());
        model.addAttribute("levels", service.getLevels());
        model.addAttribute("secretaries", secretaryService.findBySchool(school.getId()));
        if (secretaryService.findBySchool(school.getId()).size() == 0){
            return "redirect:/secretary";
        }
        return "competition/competition_add";
    }


    @GetMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String getUpdate(@PathVariable Integer id,
                            Model model) {
        model.addAttribute("competition", service.findOne(id));
        model.addAttribute("competitionDisciplines", new HashSet<>(service.findOne(id).getDisciplineTypes()));
        model.addAttribute("competitionAges", competitionDisciplineAgeService.findByCompetitionId(id));
        model.addAttribute("competitionPrograms", competitionDisciplineProgramsService.findByCompetitionId(id));
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("disciplineTypes", disciplineTypeService.all());
        model.addAttribute("competitionPrograms", competitionProgramService.all());
        model.addAttribute("ranks", rankService.all());
        model.addAttribute("school", schoolService.findOne(1));
        model.addAttribute("ageCategories", ageCategoryService.all());
        model.addAttribute("levels", service.getLevels());
        return "competition/competition_update";
    }

    @PostMapping("/{id}/confirm")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String confirm(@PathVariable Integer id){
        service.confirm(id);
        return "redirect:/competitions/" + id;
    }

    @PostMapping("/{id}/delete")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/competitions";
    }

    @GetMapping("/{id}/participants")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION') || hasAuthority('SCHOOL')")
    public String participants(@PathVariable Integer id, Model model){
        model.addAttribute("competition", service.findOne(id));
        if (applicationService.exist(id)){
            model.addAttribute("exist", "exist");
            model.addAttribute("athleteFlows", competitionOrderService.athletesFlows(id));
            model.addAttribute("teamsFlows", competitionOrderService.teamsFlows(id));
        }
        return "competition/participants";
    }

    @PostMapping("/{id}/participants")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String participants(@PathVariable Integer id, @RequestParam Integer amount){
        competitionOrderService.deleteAllByCompetitionId(id);
        competitionOrderService.athletesOrder(id, amount);
        competitionOrderService.teamsOrder(id, amount);
        return "redirect:/competitions/" + id + "/participants";
    }

    @PostMapping("/{id}/participants/{orderId}")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String changeFlow(@PathVariable Integer id, @PathVariable Integer orderId,
                             @RequestParam Integer flow){
        competitionOrderService.changeFlow(id, orderId, flow);
        return "redirect:/competitions/" + id + "/participants";
    }

    @PostMapping("/search")
    public String search(@RequestBody @ModelAttribute CompetitionFilter filter, Model model) {
        SearchModel<CompetitionFilter> searchModel = new SearchModel<>(filter, new SortingModel(), new PaginationModel());
        Page<Competition> competitions = service.search(searchModel);
        model.addAttribute("competitions", competitions.getContent());
        model.addAttribute("disciplines", disciplineService.all());
//        return new PageImpl<>(models, persons.getPageable(), persons.getTotalElements());
        return "athlete/search";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }

    @GetMapping("/file/{filename:.+}")
    public ResponseEntity<Resource> getFilePic(@PathVariable String filename) {
        {
            Resource file = storageService.loadAsResource(filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
    }
}
