package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_service.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@RequestMapping("/score")
public class CompetitionScoreController {

    private final CompetitionService competitionService;
    private final CompetitionDisciplineService competitionDisciplineService;
    private final CompetitionDisciplineProgramsService competitionDisciplineProgramsService;
    private final CompetitionDisciplineAgeService competitionDisciplineAgeService;
    private final ParticipationApplicationAthleteService applicationAthleteService;
    private final ParticipationApplicationJudgeService applicationJudgeService;
    private final ParticipationApplicationService applicationService;
    private final ScoreCriterionService criterionService;
    private final ScoreCriterionJudgeService criterionJudgeService;

    @GetMapping("/{competitionId}/work")
    public String getCompWorkingScore(@PathVariable Integer competitionId,
                                      Model model) {
        model.addAttribute("competition", competitionService.findOne(competitionId));
        model.addAttribute("applications", applicationService.allByCompetitionId(competitionId));
        model.addAttribute("judges", applicationJudgeService.allByCompetitionId(competitionId));
        model.addAttribute("programs", competitionDisciplineProgramsService.findByCompetitionId(competitionId));
        model.addAttribute("disciplineTypes", competitionDisciplineService.findByCompetitionId(competitionId));
        model.addAttribute("ageCategories", competitionDisciplineAgeService.findByCompetitionId(competitionId));
        model.addAttribute("criteria", criterionService.allByDisciplineId(competitionService.findOne(competitionId).getDiscipline().getId()));
        return "score/score_work";
    }

    @GetMapping("/{competitionId}/final")
    public String getScore(@PathVariable Integer competitionId,
                           Model model) {
        model.addAttribute("competition", competitionService.findOne(competitionId));
        model.addAttribute("applications", applicationService.allByCompetitionId(competitionId));
        model.addAttribute("programs", competitionDisciplineProgramsService.findByCompetitionId(competitionId));
        model.addAttribute("disciplineTypes", competitionDisciplineService.findByCompetitionId(competitionId));
        model.addAttribute("ageCategories", competitionDisciplineAgeService.findByCompetitionId(competitionId));
        model.addAttribute("criteria", criterionService.allByDisciplineId(competitionService.findOne(competitionId).getDiscipline().getId()));
        model.addAttribute("criterionJudges", criterionJudgeService.allByCompetitionId(competitionId));
        return "score/score_final";
    }
}
