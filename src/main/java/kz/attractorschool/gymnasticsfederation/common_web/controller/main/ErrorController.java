package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping("/403")
    public String forbidden() {
        return "error/403";
    }

    @GetMapping("/404")
    public String notFound() {
        return "error/404";
    }

    @GetMapping("/405")
    public String methotNotAllowed() {
        return "error/405";
    }

    @GetMapping("/500")
    public String internal() {
        return "error/500";
    }
}