package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_service.UserService;
import kz.attractorschool.gymnasticsfederation.dto.FederationDTO;
import kz.attractorschool.gymnasticsfederation.dto.add.FederationAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.FederationUpdateDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_service.FederationService;
import kz.attractorschool.gymnasticsfederation.exception.UserAlreadyExistsException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@AllArgsConstructor
@RequestMapping("/federation")
public class FederationController {
    private final FederationService service;
    private final PasswordEncoder encoder;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String add(){
        return "federation/federation_add";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String add(@Valid FederationAddDTO federationDTO,
                      BindingResult bindingResult,
                      RedirectAttributes attributes){
        attributes.addFlashAttribute("federationDTO", federationDTO);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/federation";
        }
        FederationDTO dto = service.add(federationDTO);
        return "redirect:/federation/" + dto.getId();
    }

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION') || hasAuthority('SCHOOL')")
    public String one(@PathVariable Integer id, Model model){
        model.addAttribute("federation", service.findOne(id));
        return "federation/federation";
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/federation";
    }

    @GetMapping("/all")
//    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION') || hasAuthority('SCHOOL')")
    public String all(Model model){
        model.addAttribute("federations", service.all());
        return "federation/federations";
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String update(@PathVariable Integer id, Model model){
        model.addAttribute("federation", service.findOne(id));
        return "federation/federation_update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String update(@PathVariable Integer id,
                         @Valid FederationUpdateDTO federationDTO,
                         BindingResult bindingResult,
                         RedirectAttributes attributes){
        attributes.addFlashAttribute("federationDTO", federationDTO);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/federation/" + id + "/update";
        }
        service.update(federationDTO, id);
        return "redirect:/federation/" + id;
    }

    @GetMapping("/{id}/update-email")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String updateEmail(@PathVariable Integer id, Model model){
        model.addAttribute("federation", service.findOne(id));
        return "federation/federation_update_email";
    }

    @PostMapping("/{id}/update-email")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String updateEmail(@PathVariable Integer id,
                              @Valid UserRegisterDTO dto,
                              BindingResult bindingResult,
                              RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (!service.findOne(id).getUser().getEmail().equals(dto.getEmail()) ||
                !encoder.matches(dto.getOldPassword(), service.findOne(id).getUser().getPassword())){
            attributes.addFlashAttribute("emailError", "Проверьте старый пароль или старый мейл. Пользователь не найден");
            return "redirect:/federation/" + id + "/update-email";
        }
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/federation/" + id + "/update-email";
        }
        service.updateEmail(dto, id);
        return "redirect:/federation/" + id;
    }

    @GetMapping("/{id}/update-password")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String updatePassword(@PathVariable Integer id, Model model){
        model.addAttribute("federation", service.findOne(id));
        return "federation/federation_update_password";
    }

    @PostMapping("/{id}/update-password")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String updatePassword(@PathVariable Integer id,
                                 @Valid UserPasswordDTO dto,
                                 BindingResult bindingResult,
                                 RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/federation/" + id + "/update-password";
        }
        if (!encoder.matches(dto.getOldPassword(), service.findOne(id).getUser().getPassword())){
            attributes.addFlashAttribute("emailError", "Вы ввели неверный пароль");
            return "redirect:/federation/" + id + "/update-password";
        }
        service.updatePassword(dto, id);
        return "redirect:/federation/" + id;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    private String handleRNF(UserAlreadyExistsException ex, Model model) {
        model.addAttribute("result", ex.getResult());
        return "exception/resource-not-found";
    }
}
