package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_data.specification.PaginationModel;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SearchModel;
import kz.attractorschool.gymnasticsfederation.common_data.specification.SortingModel;
import kz.attractorschool.gymnasticsfederation.dto.*;
import kz.attractorschool.gymnasticsfederation.dto.add.JudgeAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.search.JudgeFilter;
import kz.attractorschool.gymnasticsfederation.dto.update.JudgeUpdateDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.exception.StorageException;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.*;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Judge;
import kz.attractorschool.gymnasticsfederation.common_service.*;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/judge")
public class JudgeController {
    private final JudgeService service;
    private final SchoolService schoolService;
    private final JudgeCategoryService categoryService;
    private final DisciplineService disciplineService;
    private final PersonService personService;
    private final FileSystemStorageService fileSystemStorageService;
    private final StorageService storageService;
    private final UserService userService;

    @GetMapping("/all")
    public String all(Model model){
        List<Judge> judges = service.all();
        model.addAttribute("judges", judges);
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("categories", categoryService.all());
        return "judge/judges";
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String add(Model model, Principal principal){
        model.addAttribute("school", schoolService.findByUser(userService.findByEmail(principal.getName())));
        model.addAttribute("categories", categoryService.all());
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("persons", personService.all());
        return "judge/judge_add";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String add(@Valid JudgeAddDTO judgeAddDTO,
                      BindingResult result,
                      RedirectAttributes attributes,
                      @RequestParam("categoryFile")MultipartFile categoryFile){
        attributes.addFlashAttribute("judgeDTO", judgeAddDTO);
        if (result.hasFieldErrors()){
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/judge";
        }
        if (!service.isPdf(categoryFile)){
            attributes.addFlashAttribute("filesError", "Все файлы должны быть в формате PDF");
            return "redirect:/judge";
        }
        JudgeCategoryFile category = new JudgeCategoryFile(categoryFile.getOriginalFilename());
        fileSystemStorageService.store(categoryFile);
        JudgeDTO dto = service.add(judgeAddDTO, category);
        return "redirect:/judge/" + dto.getId();
    }

    @GetMapping("/{id}")
    public String one(@PathVariable Integer id, Model model){
        JudgeDTO judgeDTO = service.getOne(id);
        model.addAttribute("judge", judgeDTO);
        return "judge/judge";
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/judge/all";
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String update(@PathVariable Integer id, Model model){
        model.addAttribute("judge", service.getOne(id));
        model.addAttribute("schools", schoolService.all());
        model.addAttribute("categories", categoryService.all());
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("persons", personService.all());
        model.addAttribute("judge", service.findOne(id));
        return "judge/judge_update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String update(@PathVariable Integer id,
                         @Valid JudgeUpdateDTO judgeUpdateDTO,
                         BindingResult result,
                         RedirectAttributes attributes,
                         @RequestParam("categoryFile")MultipartFile categoryFile){
        attributes.addFlashAttribute("judgeDTO", judgeUpdateDTO);
        if (result.hasFieldErrors()){
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/judge";
        }
        if (categoryFile != null && !categoryFile.isEmpty()){
            if (!service.isPdf(categoryFile)){
                attributes.addFlashAttribute("filesError", "Все файлы должны быть в формате PDF");
                return "redirect:/judge/" + id + "/update";
            }
            JudgeCategoryFile category = new JudgeCategoryFile(categoryFile.getOriginalFilename());
            fileSystemStorageService.store(categoryFile);
            service.updateFile(id, category);
        }

        JudgeDTO dto = service.update(id, judgeUpdateDTO);
        return "redirect:/judge/" + dto.getId();
    }

    @GetMapping("/search")
    public String searchAll(Model model){
        List<Judge> judges = service.all();
        model.addAttribute("judges", judges);
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("categories", categoryService.all());
        return "judge/judges";
    }

    @PostMapping("/search")
    public String search(@RequestBody @ModelAttribute JudgeFilter filter, Model model) {
        SearchModel<JudgeFilter> searchModel = new SearchModel<>(filter, new SortingModel(), new PaginationModel());
        Page<Judge> judges = service.search(searchModel);
        model.addAttribute("judges", judges.getContent());
        model.addAttribute("categories", categoryService.all());
        model.addAttribute("disciplines", disciplineService.all());
//        return new PageImpl<>(models, persons.getPageable(), persons.getTotalElements());
        return "judge/search";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }

    @ExceptionHandler(StorageException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private String handleRNF(StorageException ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        model.addAttribute("cause", ex.getCause());
        return "exception/empty_file";
    }

    @GetMapping("/file/{filename:.+}")
    public ResponseEntity<Resource> getFilePic(@PathVariable String filename) {
        {
            Resource file = storageService.loadAsResource(filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
    }
}
