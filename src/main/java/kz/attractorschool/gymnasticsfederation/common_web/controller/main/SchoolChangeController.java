package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_data.entity.SchoolChange;
import kz.attractorschool.gymnasticsfederation.common_service.AthleteService;
import kz.attractorschool.gymnasticsfederation.common_service.CoachService;
import kz.attractorschool.gymnasticsfederation.common_service.JudgeService;
import kz.attractorschool.gymnasticsfederation.common_service.SchoolChangeService;
import kz.attractorschool.gymnasticsfederation.dto.add.SchoolChangeAddDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/change-school")
@RequiredArgsConstructor
public class SchoolChangeController {
    private final SchoolChangeService service;
    private final AthleteService athleteService;
    private final CoachService coachService;
    private final JudgeService judgeService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String all(Model model){
        model.addAttribute("schoolChanges", service.all());
        return "school_change/school_changes";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String changeSchool(@Valid SchoolChangeAddDTO dto,
                               BindingResult result,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", dto);
        boolean isAthlete = dto.getAthleteId() != null;
        boolean isCoach = dto.getCoachId() != null;
        if (result.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            if (isAthlete){
                return "redirect:/athlete/" + dto.getAthleteId() + "/update";
            }
            else if (isCoach){
                return "redirect:/coach/" + dto.getCoachId() + "/update";
            }
            else{
                return "redirect:/judge/" + dto.getCoachId() + "/update";
            }
        }
        SchoolChange schoolChange;
        if (isAthlete){
            schoolChange = service.add(dto, athleteService.findOne(dto.getAthleteId()));
        }
        else if (isCoach){
            schoolChange = service.add(dto, coachService.findOne(dto.getCoachId()));
        }
        else{
            schoolChange = service.add(dto, judgeService.findOne(dto.getJudgeId()));
        }

        return "redirect:/change-school/" + schoolChange.getId();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION') || hasAuthority('SCHOOL')")
    public String getOne(@PathVariable Integer id, Model model){
        model.addAttribute("schoolChange", service.findOne(id));
        return "school_change/school_change";
    }

    @PostMapping("/{id}/confirm")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String confirm (@PathVariable Integer id,
                           RedirectAttributes attributes){
        service.confirm(id);
        attributes.addFlashAttribute("result", "Заявка подтверждена успешно");
        return "redirect:/change-school/" + id;
    }

    @PostMapping("/{id}/cancel")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String cancel (@PathVariable Integer id,
                           RedirectAttributes attributes){
        service.cancel(id);
        attributes.addFlashAttribute("result", "Заявка отменена успешно");
        return "redirect:/change-school/" + id;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }
}
