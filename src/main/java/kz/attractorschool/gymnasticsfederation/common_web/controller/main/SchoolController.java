package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_service.UserService;
import kz.attractorschool.gymnasticsfederation.dto.SchoolDTO;
import kz.attractorschool.gymnasticsfederation.dto.add.SchoolAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.SchoolUpdateDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_service.FederationService;
import kz.attractorschool.gymnasticsfederation.common_service.SchoolService;
import kz.attractorschool.gymnasticsfederation.exception.UserAlreadyExistsException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;


@Controller
@AllArgsConstructor
@RequestMapping("/school")
public class SchoolController {
    private final SchoolService service;
    private final FederationService federationService;
    private final PasswordEncoder encoder;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String add(Model model){
        model.addAttribute("federations", federationService.all());
        return "school/school_add";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String add(@Valid SchoolAddDTO schoolDTO,
                      BindingResult bindingResult,
                      RedirectAttributes attributes){
        attributes.addFlashAttribute("schoolDTO", schoolDTO);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/school";
        }
        SchoolDTO dto = service.add(schoolDTO);
        return "redirect:/school/" + dto.getId();
    }

    @GetMapping("/{id}")
    public String one(@PathVariable Integer id, Model model){
        model.addAttribute("school", service.findOne(id));
        return "school/school";
    }

    @GetMapping("/all")
    public String all(Model model){
        model.addAttribute("schools", service.all());
        return "school/all";
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/school";
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION') || hasAuthority('SCHOOL')")
    public String update(@PathVariable Integer id, Model model){
        model.addAttribute("school", service.findOne(id));
        model.addAttribute("federations", federationService.all());
        return "school/school_update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION') || hasAuthority('SCHOOL')")
    public String update(@PathVariable Integer id,
                         @Valid SchoolUpdateDTO schoolDTO,
                         BindingResult bindingResult,
                         RedirectAttributes attributes){
        attributes.addFlashAttribute("schoolDTO", schoolDTO);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/school/" + id + "/update";
        }
        service.update(schoolDTO, id);
        return "redirect:/school/" + id;
    }

    @GetMapping("/{id}/update-email")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String updateEmail(@PathVariable Integer id, Model model){
        model.addAttribute("school", service.findOne(id));
        return "school/school_update_email";
    }

    @PostMapping("/{id}/update-email")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String updateEmail(@PathVariable Integer id,
                              @Valid UserRegisterDTO dto,
                              BindingResult bindingResult,
                              RedirectAttributes attributes, Principal principal){
        attributes.addFlashAttribute("dto", dto);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/school/" + id + "/update-email";
        }
        if (!service.findOne(id).getUser().getEmail().equals(dto.getEmail()) ||
                !encoder.matches(dto.getOldPassword(), service.findOne(id).getUser().getPassword())){
            attributes.addFlashAttribute("emailError", "Проверьте старый пароль или старый мейл. Пользователь не найден");
            return "redirect:/school/" + id + "/update-email";
        }
        service.updateEmail(dto, id);
        return "redirect:/school/" + id;
    }

    @GetMapping("/{id}/update-password")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String updatePassword(@PathVariable Integer id, Model model){
        model.addAttribute("school", service.findOne(id));
        return "school/school_update_password";
    }

    @PostMapping("/{id}/update-password")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
    public String updatePassword(@PathVariable Integer id,
                                 @Valid UserPasswordDTO dto,
                                 BindingResult bindingResult,
                                 RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/school/" + id + "/update-password";
        }
        if (!encoder.matches(dto.getOldPassword(), service.findOne(id).getUser().getPassword())){
            attributes.addFlashAttribute("emailError", "Вы ввели неверный пароль");
            return "redirect:/school/" + id + "/update-password";
        }
        service.updatePassword(dto, id);
        return "redirect:/school/" + id;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }


    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    private String handleRNF(UserAlreadyExistsException ex, Model model) {
        model.addAttribute("result", ex.getResult());
        return "exception/resource-not-found";
    }
}
