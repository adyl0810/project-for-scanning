package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterion;
import kz.attractorschool.gymnasticsfederation.common_service.DisciplineService;
import kz.attractorschool.gymnasticsfederation.common_service.DisciplineTypeService;
import kz.attractorschool.gymnasticsfederation.common_service.ScoreCriterionService;
import kz.attractorschool.gymnasticsfederation.dto.add.ScoreCriterionAddDTO;
import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/criteria")
@PreAuthorize("hasAuthority('ADMIN') || hasAuthority('FEDERATION')")
public class ScoreCriterionController {
    private final ScoreCriterionService service;
    private final DisciplineTypeService disciplineTypeService;
    private final DisciplineService disciplineService;

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("disciplines", disciplineService.all());
        return "criterion/criterion_add";
    }

    @PostMapping("/add")
    public String add(@Valid ScoreCriterionAddDTO dto,
                      BindingResult result,
                      RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", dto);
        if (result.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/criteria/add";
        }
        ScoreCriterion criterion = service.add(dto);
        return "redirect:/criteria/" + criterion.getId();
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Integer id, Model model){
        model.addAttribute("criterion", service.findOne(id));
        return "criterion/criterion";
    }

    @PostMapping("/{id}")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/criteria";
    }

    @GetMapping("/{id}/update")
    public String update(@PathVariable Integer id, Model model){
        model.addAttribute("criterion", service.findOne(id));
        model.addAttribute("disciplines", disciplineService.all());
        return "criterion/criterion_update";
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable Integer id,
                         @Valid ScoreCriterionAddDTO dto,
                         BindingResult result,
                         RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", dto);
        if (result.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/criteria/" + id + "/update";
        }
        service.update(id, dto);
        return "redirect:/criteria/" + id;
    }

    @GetMapping
    public String all(Model model){
        model.addAttribute("disciplines", disciplineService.all());
        model.addAttribute("criteria", service.all());
        return "criterion/criteria";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }
}
