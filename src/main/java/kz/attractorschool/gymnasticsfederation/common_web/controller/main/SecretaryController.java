package kz.attractorschool.gymnasticsfederation.common_web.controller.main;

import kz.attractorschool.gymnasticsfederation.common_data.entity.School;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Secretary;
import kz.attractorschool.gymnasticsfederation.common_service.SchoolService;
import kz.attractorschool.gymnasticsfederation.common_service.SecretaryService;
import kz.attractorschool.gymnasticsfederation.common_service.UserService;
import kz.attractorschool.gymnasticsfederation.dto.add.SecretaryAddDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.SecretaryUpdateDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserPasswordDTO;
import kz.attractorschool.gymnasticsfederation.dto.update.UserRegisterDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
@RequestMapping("/secretary")
@PreAuthorize("hasAuthority('ADMIN') || hasAuthority('SCHOOL')")
public class SecretaryController {
    private final SecretaryService service;
    private final UserService userService;
    private final SchoolService schoolService;
    private final PasswordEncoder encoder;

    @GetMapping("/all")
    public String all(Model model, Principal principal){
        School school = schoolService.findByUser(userService.findByEmail(principal.getName()));
        model.addAttribute("secretaries", service.findBySchool(school.getId()));
        return "secretary/secretaries";
    }

    @GetMapping
    public String add(Model model, Principal principal){
        model.addAttribute("school", schoolService.findByUser(userService.findByEmail(principal.getName())));
        return "secretary/secretary_add";
    }

    @PostMapping
    public String add(@Valid SecretaryAddDTO dto,
                      BindingResult result,
                      RedirectAttributes attributes){
        attributes.addFlashAttribute("secretaryDTO", dto);
        if (result.hasFieldErrors()){
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/secretary";
        }
        Secretary secretary = service.add(dto);
        return "redirect:/secretary/" + secretary.getId();
    }

    @GetMapping("/{id}")
    public String getOne(Model model, @PathVariable Integer id){
        model.addAttribute("secretary", service.findOne(id));
        return "secretary/secretary";
    }

    @GetMapping("/{id}/update")
    public String update(Model model, @PathVariable Integer id){
        model.addAttribute("secretary", service.findOne(id));
        return "secretary/secretary_update";
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable Integer id, @Valid SecretaryUpdateDTO dto,
                         BindingResult result,
                         RedirectAttributes attributes){
        attributes.addFlashAttribute("secretaryDTO", dto);
        if (result.hasFieldErrors()){
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/secretary/" + id + "/update";
        }
        service.update(dto, id);
        return "redirect:/secretary/" + id;
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/secretary";
    }

    @GetMapping("/{id}/update-email")
    public String updateEmail(@PathVariable Integer id, Model model){
        model.addAttribute("secretary", service.findOne(id));
        return "secretary/secretary_update_email";
    }

    @PostMapping("/{id}/update-email")
    public String updateEmail(@PathVariable Integer id,
                              @Valid UserRegisterDTO dto,
                              BindingResult bindingResult,
                              RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/secretary/" + id + "/update-email";
        }
        if (!service.findOne(id).getUser().getEmail().equals(dto.getEmail()) ||
                !encoder.matches(dto.getOldPassword(), service.findOne(id).getUser().getPassword())){
            attributes.addFlashAttribute("emailError", "Проверьте старый пароль или старый мейл. Пользователь не найден");
            return "redirect:/secretary/" + id + "/update-email";
        }
        service.updateEmail(dto, id);
        return "redirect:/school/" + id;
    }

    @GetMapping("/{id}/update-password")
    public String updatePassword(@PathVariable Integer id, Model model){
        model.addAttribute("secretary", service.findOne(id));
        return "secretary/secretary_update_password";
    }

    @PostMapping("/{id}/update-password")
    public String updatePassword(@PathVariable Integer id,
                                 @Valid UserPasswordDTO dto,
                                 BindingResult bindingResult,
                                 RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (bindingResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/secretary/" + id + "/update-password";
        }
        if (!encoder.matches(dto.getOldPassword(), service.findOne(id).getUser().getPassword())){
            attributes.addFlashAttribute("emailError", "Вы ввели неверный пароль");
            return "redirect:/secretary/" + id + "/update-password";
        }
        service.updatePassword(dto, id);
        return "redirect:/secretary/" + id;
    }
}
