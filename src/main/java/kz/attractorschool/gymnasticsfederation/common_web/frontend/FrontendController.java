package kz.attractorschool.gymnasticsfederation.common_web.frontend;

import kz.attractorschool.gymnasticsfederation.exception.ResourceNotFoundException;
import kz.attractorschool.gymnasticsfederation.common_service.*;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping
@AllArgsConstructor
public class  FrontendController {

    private final DisciplineService disciplineService;
    private final AthleteService athleteService;
    private final JudgeCategoryService judgeCategoryService;
    private final CoachCategoryService coachCategoryService;
    private final RankService rankService;
    private final PersonService personService;
    private final DisciplineTypeService disciplineTypeService;
    private final CompetitionProgramService competitionProgramService;
    private final SchoolService schoolService;
    private final AgeCategoryService ageCategoryService;
    private final CompetitionService competitionService;
    private final CompetitionDisciplineAgeService competitionDisciplineAgeService;
    private final CompetitionDisciplineProgramsService competitionDisciplineProgramsService;
    private final ParticipationApplicationService participationApplicationService;
    private final ParticipationApplicationAthleteService participationApplicationAthleteService;
    private final CompetitionDisciplineService competitionDisciplineService;
    private final ParticipationApplicationService applicationService;
    private final ScoreCriterionService criterionService;
    private final SecretaryService secretaryService;

    @GetMapping
    public String index() {
        return "index";
    }

    @GetMapping("/competition/{id}/score")
    public String getScore(@PathVariable Integer id,
                           Model model) {
        model.addAttribute("competition", competitionService.findOne(id));
        model.addAttribute("applications", applicationService.allByCompetitionId(id));
        model.addAttribute("programs", competitionDisciplineProgramsService.findByCompetitionId(id));
        model.addAttribute("disciplineTypes", competitionDisciplineService.findByCompetitionId(id));
        model.addAttribute("ageCategories", competitionDisciplineAgeService.findByCompetitionId(id));
        model.addAttribute("criteria", criterionService.allByDisciplineId(competitionService.findOne(id).getDiscipline().getId()));
        return "score_final";
    }

    @GetMapping("/test")
    public String test() {
        return "partials/templates";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF(ResourceNotFoundException ex, Model model) {
        model.addAttribute("resource", ex.getResource());
        model.addAttribute("id", ex.getId());
        return "exception/resource-not-found";
    }
}
