package kz.attractorschool.gymnasticsfederation.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import kz.attractorschool.gymnasticsfederation.common_data.entity.*;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionScoreDTO {
    private Integer id;
    private double score;
    private ScoreCriterionDTO criterion;
    private CompetitionDisciplineDTO disciplineType;
    private CompetitionDisciplineProgramDTO program;
    private CompetitionDisciplineAgeDTO ageCategory;
    private ParticipationApplicationAthleteDTO athlete;
//    private JudgeDTO judge;

    public static CompetitionScoreDTO from(CompetitionScore score){
        return CompetitionScoreDTO.builder()
                .id(score.getId())
                .score(score.getScore())
                .criterion(ScoreCriterionDTO.from(score.getCriterion()))
                .disciplineType(CompetitionDisciplineDTO.from(score.getDisciplineType()))
                .program(CompetitionDisciplineProgramDTO.from(score.getProgram()))
                .ageCategory(CompetitionDisciplineAgeDTO.from(score.getAgeCategory()))
                .athlete(ParticipationApplicationAthleteDTO.from(score.getAthlete()))
//                .judge(JudgeDTO.from(score.getJudge()))
                .build();
    }
}
