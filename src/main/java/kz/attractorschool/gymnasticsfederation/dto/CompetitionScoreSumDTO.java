package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.CompetitionScoreSum;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionScoreSumDTO {
    private Integer id;
    private double sum;
    private CompetitionDisciplineDTO disciplineType;
    private CompetitionDisciplineProgramDTO program;
    private CompetitionDisciplineAgeDTO ageCategory;
    private ParticipationApplicationAthleteDTO athlete;

    public static CompetitionScoreSumDTO from(CompetitionScoreSum scoreSum){
        return CompetitionScoreSumDTO.builder()
                .id(scoreSum.getId())
                .sum(scoreSum.getSum())
                .disciplineType(CompetitionDisciplineDTO.from(scoreSum.getDisciplineType()))
                .program(CompetitionDisciplineProgramDTO.from(scoreSum.getProgram()))
                .ageCategory(CompetitionDisciplineAgeDTO.from(scoreSum.getAgeCategory()))
                .athlete(ParticipationApplicationAthleteDTO.from(scoreSum.getAthlete()))
                .build();
    }
}
