package kz.attractorschool.gymnasticsfederation.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionScoreSumGetDTO {
    private int disciplineTypeId;
    private int programId;
    private int ageCategoryId;
    private int athleteId;
}
