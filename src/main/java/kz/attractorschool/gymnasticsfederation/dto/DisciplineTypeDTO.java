package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.DisciplineType;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DisciplineTypeDTO {
    private Integer id;
    private String name;
    @Builder.Default
    private boolean isDel = false;
    private DisciplineDTO discipline;
    private Integer participantsAmountMin;
    private Integer participantsAmountMax;
    private String gender;

    public static DisciplineTypeDTO from(DisciplineType disciplineType) {
        return DisciplineTypeDTO.builder()
                .id(disciplineType.getId())
                .name(disciplineType.getName())
                .participantsAmountMax(disciplineType.getParticipantsAmountMax())
                .participantsAmountMin(disciplineType.getParticipantsAmountMin())
                .gender(disciplineType.getGender().getName())
                .isDel(disciplineType.isDel())
                .discipline(DisciplineDTO.from(disciplineType.getDiscipline()))
                .build();
    }
}
