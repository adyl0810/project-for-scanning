package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Federation;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class FederationDTO {
    private Integer id;
    private String name;
    private String director;
    private String email;
    private String password;
    private String address;
    private String phone;
    private boolean isDel;

    public static FederationDTO from(Federation federation){
        return FederationDTO.builder()
                .id(federation.getId())
                .name(federation.getName())
                .director(federation.getDirector())
                .address(federation.getAddress())
                .phone(federation.getPhone())
                .isDel(federation.isDel())
                .build();
    }
}
