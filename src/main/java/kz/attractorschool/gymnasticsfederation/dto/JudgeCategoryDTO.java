package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.JudgeCategory;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JudgeCategoryDTO {
    private Integer id;

    @NotNull(message = "Вы ввели пустое значение")
    @Size(min = 1, message = "Вы ввели пустое значение")
    private String name;

    private boolean isDel;

    public static JudgeCategoryDTO from(JudgeCategory judgeCategory){
        return JudgeCategoryDTO.builder()
                .id(judgeCategory.getId())
                .name(judgeCategory.getName())
                .isDel(judgeCategory.isDel())
                .build();
    }
}
