package kz.attractorschool.gymnasticsfederation.dto;

import com.sun.istack.NotNull;
import kz.attractorschool.gymnasticsfederation.common_data.entity.files.PersonPhoto;
import kz.attractorschool.gymnasticsfederation.common_data.entity.Person;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.parameters.P;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonDTO {
    private Integer id;

    @NotBlank
    @Size(min = 1, message = "Минимальная длина фамилии - один символ")
    private String surname;

    @NotBlank
    @Size(min = 2, message = "Минимальная длина имени - два символа")
    private String name;

    private String middleName;

    private PersonPhoto photo;

    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "Выберите дату рождения")
    @NotNull
    private String birthday;

    private LocalDate birthdayDate;

    @NotNull
    @Size(min = 12, max = 12, message = "Длина ИИН - 12 символов")
    @Pattern(regexp = "\\d+", message = "ИИн не может содержать буквы")
    private String iin;

    @NotNull
    private String gender;

    @NotNull
    @NotBlank
    @Size(min = 2, message = "Слишком короткое имя города")
    private String city;

    @NotNull
    @NotBlank
    @Size(min = 5, message = "Вы ввели слишком короткий адрес")
    private String address;

    @NotNull
    @Pattern(regexp = "^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$", message = "Введите номер телефона в формате +70001112233")
    private String phone;

    @NotNull
    @Pattern(regexp = "([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})", message = "Введите корректный мэйл")
    private String email;

    @NotNull
    @NotBlank
    @Size(min = 2, message = "Слишком короткий ввод, введите полное название")
    private String education;

    private String comment;

    private boolean isDel;

    public static PersonDTO from(Person person){
        return PersonDTO.builder()
                .id(person.getId())
                .surname(person.getSurname())
                .name(person.getName())
                .middleName(person.getMiddleName())
                .photo(person.getPhoto())
                .birthdayDate(person.getBirthday())
                .iin(person.getIin())
                .gender(person.getGender().getName())
                .city(person.getCity())
                .address(person.getAddress())
                .phone(person.getPhone())
                .email(person.getEmail())
                .education(person.getEducation())
                .comment(person.getComment())
                .isDel(person.isDel())
                .build();
    }
}
