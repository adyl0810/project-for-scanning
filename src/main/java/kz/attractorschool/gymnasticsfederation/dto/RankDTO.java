package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.Rank;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RankDTO {
    private Integer id;

    @NotNull(message = "Вы ввели пустое значение")
    @Size(min = 1, message = "Вы ввели пустое значение")
    private String name;

    private boolean isDel;

    public static RankDTO from(Rank rank){
        return RankDTO.builder()
                .id(rank.getId())
                .name(rank.getName())
                .isDel(rank.isDel())
                .build();
    }
}
