package kz.attractorschool.gymnasticsfederation.dto;


import com.sun.istack.NotNull;
import kz.attractorschool.gymnasticsfederation.common_data.entity.School;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class SchoolDTO {
    private Integer id;
    private String name;
    private String director;
    private String email;
    private String password;
    private String address;
    private String phone;

    @ToString.Exclude
    private FederationDTO federation;

    @ToString.Exclude
    private UserDTO user;

    private boolean isDel;

    public static SchoolDTO from(School school){
        return SchoolDTO.builder()
                .id(school.getId())
                .name(school.getName())
                .director(school.getDirector())
                .address(school.getAddress())
                .phone(school.getPhone())
                .federation(FederationDTO.from(school.getFederation()))
                .user(UserDTO.from(school.getUser()))
                .isDel(school.isDel())
                .build();
    }
}
