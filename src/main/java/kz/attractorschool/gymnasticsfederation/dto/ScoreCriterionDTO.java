package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterion;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ScoreCriterionDTO {
    private Integer id;
    private String name;
    private Integer minScore;
    private Integer maxStore;
    private DisciplineDTO discipline;

    public static ScoreCriterionDTO from(ScoreCriterion criterion){
        return ScoreCriterionDTO.builder()
                .id(criterion.getId())
                .name(criterion.getName())
                .minScore(criterion.getMinScore())
                .maxStore(criterion.getMaxScore())
                .discipline(DisciplineDTO.from(criterion.getDiscipline()))
                .build();
    }
}
