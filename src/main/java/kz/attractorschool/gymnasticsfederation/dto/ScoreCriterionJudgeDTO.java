package kz.attractorschool.gymnasticsfederation.dto;

import kz.attractorschool.gymnasticsfederation.common_data.entity.ScoreCriterionJudge;
import lombok.*;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ScoreCriterionJudgeDTO {

    private Integer id;
    private ParticipationApplicationJudgeDTO judge;
    private ScoreCriterionDTO criterion;

    public static ScoreCriterionJudgeDTO from(ScoreCriterionJudge criterionJudge) {
        return ScoreCriterionJudgeDTO.builder()
                .id(criterionJudge.getId())
                .judge(ParticipationApplicationJudgeDTO.from(criterionJudge.getJudge()))
                .criterion(ScoreCriterionDTO.from(criterionJudge.getCriterion()))
                .build();
    }
}
