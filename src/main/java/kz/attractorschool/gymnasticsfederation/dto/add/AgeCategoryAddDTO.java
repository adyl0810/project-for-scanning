package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AgeCategoryAddDTO {
    private Integer minYear;
    private Integer maxYear;

    @NotNull(message = "Выберите дисциплину")
    @Min(value = 1, message = "Выберите дисциплину")
    private Integer disciplineId;
    private Integer rankId;
}
