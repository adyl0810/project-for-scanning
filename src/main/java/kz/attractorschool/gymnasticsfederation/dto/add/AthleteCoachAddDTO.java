package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class AthleteCoachAddDTO {
    @NotNull
    @Min(value = 1, message = "Выберите тренера")
    private int coachId;

    @NotNull
    @Size(min = 1, message = "Выберите роль")
    private String role;

    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "Выберите дату регистрации")
    @NotNull
    private String registerDate;
}
