package kz.attractorschool.gymnasticsfederation.dto.add;

import com.sun.istack.NotNull;
import lombok.*;

import javax.validation.constraints.Min;

@Getter
@Setter
@AllArgsConstructor
public class CoachAddDTO {
    @NotNull
    @Min(value = 1, message = "Выберите персону")
    private Integer personId;

    @NotNull
    @Min(value = 1, message = "Выберите школу")
    private Integer schoolId;

    @NotNull
    @Min(value = 1, message = "Выберите вид спорта")
    private Integer disciplineId;

    @NotNull
    @Min(value = 1, message = "Выберите разряд (Доступен вариант 'Нет разряда')")
    private Integer categoryId;
}
