package kz.attractorschool.gymnasticsfederation.dto.add;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionAddDTO {

    private Integer id;

    @NotNull
    @Size(min = 2, message = "Введите название соревнования")
    private String name;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate finishDate;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate participationDate;

    @NotNull
    private String level;

    @NotNull
    private String country;

    @NotNull
    private String city;

    @NotNull
    private String address;

    @NotNull
    private String areaName;

    @NotNull
    private String contactName;

    @NotNull
    private String contactPhone;

    @NotNull
    @Min(1)
    private Integer disciplineId;

    @NotNull
    @Min(1)
    private Integer secretaryId;

    @NotNull
    @Min(1)
    private Integer schoolId;
}
