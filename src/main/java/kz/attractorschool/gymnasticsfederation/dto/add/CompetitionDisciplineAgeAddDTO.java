package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionDisciplineAgeAddDTO {

    private Integer id;

    @NotNull
    private int teamChampionship;

    @NotNull
    private Integer competitionId;

    private Integer disciplineTypeId;

    private Integer ageCategoryId;

    private Integer maxTeams;

    private Integer maxAthletes;
}
