package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionDisciplineProgramAddDTO {

    private Integer id;

    @NotNull
    private Integer competitionId;

    private Integer disciplineTypeId;

    private Integer competitionProgramId;
}
