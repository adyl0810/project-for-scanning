package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionScoreAddDTO {
    private double score;
    private int criterionId;
    private int disciplineTypeId;
    private int programId;
    private int ageCategoryId;
    private int athleteId;
//    private int judgeId;
}
