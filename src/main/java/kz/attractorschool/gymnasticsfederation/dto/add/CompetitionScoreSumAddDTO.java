package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionScoreSumAddDTO {
    private double sum;
    private int disciplineTypeId;
    private int programId;
    private int ageCategoryId;
    private int athleteId;
}
