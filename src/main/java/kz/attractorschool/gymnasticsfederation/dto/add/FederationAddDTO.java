package kz.attractorschool.gymnasticsfederation.dto.add;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class FederationAddDTO {
    @NotNull
    @Size(min = 5, message = "Введите полное название школы")
    private String name;

    @Column
    @NotNull
    @Size(min = 5, message = "Введите полные данные о директоре")
    private String director;

    @NotNull
    @Pattern(regexp = "([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})", message = "Введите корректный мэйл")
    private String email;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$", message = "Пароль должен содержать букву в верхнем регистре, нижнем регистре, цифру, длина - 8 символов")
    private String password;

    @NotNull
    @NotBlank
    @Size(min = 5, message = "Введите полный адрес")
    private String address;

    @NotNull
    @Pattern(regexp = "^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$", message = "Введите номер телефона в формате +70001112233")
    private String phone;
}
