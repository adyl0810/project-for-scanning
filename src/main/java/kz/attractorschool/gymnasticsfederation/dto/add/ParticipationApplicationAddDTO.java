package kz.attractorschool.gymnasticsfederation.dto.add;

import com.sun.istack.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParticipationApplicationAddDTO {
    @NotNull
    private Integer competitionId;

    @NotNull
    private Integer schoolId;
}
