package kz.attractorschool.gymnasticsfederation.dto.add;

import com.sun.istack.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParticipationApplicationAthleteAddDTO {
    @NotNull
    private Integer applicationId;

    @NotNull
    private Integer athleteId;

    @NotNull
    private Integer disciplineAgeId;

    @NotNull
    private Integer disciplineTypeId;

    private Integer teamNumber;
}
