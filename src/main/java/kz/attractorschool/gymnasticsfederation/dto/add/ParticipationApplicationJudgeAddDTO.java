package kz.attractorschool.gymnasticsfederation.dto.add;

import com.sun.istack.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParticipationApplicationJudgeAddDTO {
    @NotNull
    private Integer applicationId;

    @NotNull
    private Integer judgeId;
}
