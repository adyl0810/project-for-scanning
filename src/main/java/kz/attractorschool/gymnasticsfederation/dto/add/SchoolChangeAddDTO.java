package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SchoolChangeAddDTO {
    @NotNull
    @Min(value = 1, message = "Выберите новую школу")
    private Integer newSchool;
    private Integer athleteId;
    private Integer coachId;
    private Integer judgeId;
}
