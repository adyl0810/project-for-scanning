package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ScoreCriterionAddDTO {
    @NotNull
    @Size(min = 3, message = "Введите полное наименование")
    private String name;
    private Integer minScore;
    private Integer maxScore;

    @NotNull
    @Min(value = 1, message = "Выберите соревновательную программу")
    private Integer disciplineId;

    private Integer disciplineTypeId;
}
