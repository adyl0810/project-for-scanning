package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ScoreCriterionJudgeAddDTO {

    @NotNull
    @Min(value = 1, message = "Выберите судью")
    private Integer judgeId;

    @NotNull
    @Min(value = 1, message = "Выберите критерий")
    private Integer criterionId;
}
