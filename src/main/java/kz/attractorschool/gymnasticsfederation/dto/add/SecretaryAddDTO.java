package kz.attractorschool.gymnasticsfederation.dto.add;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SecretaryAddDTO {
    @NotNull
    @Size(min = 1, message = "Введите имя")
    private String name;

    @NotNull
    @Size(min = 1, message = "Введите фамилию")
    private String surname;

    @NotNull
    @Pattern(regexp = "([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})", message = "Введите корректный мэйл")
    private String email;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$", message = "Пароль должен содержать букву в верхнем регистре, нижнем регистре, цифру, длина - 8 символов")
    private String password;

    @NotNull
    @Min(value = 1, message = "Выберите школу")
    private Integer schoolId;
}
