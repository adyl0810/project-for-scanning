package kz.attractorschool.gymnasticsfederation.dto.search;

import kz.attractorschool.gymnasticsfederation.common_data.specification.FilterModel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
public class CompetitionFilter implements FilterModel {
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate applicationDate;

    private Integer disciplineId;
    private String city;
}
