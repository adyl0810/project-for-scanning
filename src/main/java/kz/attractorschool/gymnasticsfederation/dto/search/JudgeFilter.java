package kz.attractorschool.gymnasticsfederation.dto.search;

import kz.attractorschool.gymnasticsfederation.common_data.specification.FilterModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JudgeFilter implements FilterModel {
    private String name;
    private String surname;
    private Integer disciplineId;
    private Integer categoryId;
    private String registryNum;
    private String city;
}
