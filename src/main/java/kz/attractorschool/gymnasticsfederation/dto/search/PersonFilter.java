package kz.attractorschool.gymnasticsfederation.dto.search;

import kz.attractorschool.gymnasticsfederation.common_data.specification.FilterModel;
import lombok.*;

@Getter
@Setter
public class PersonFilter implements FilterModel {
    private String name;
    private String surname;
    private String iin;
    private String city;
}
