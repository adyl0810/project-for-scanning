package kz.attractorschool.gymnasticsfederation.dto.update;

import com.sun.istack.NotNull;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class AthleteUpdateDTO {
    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "Выберите дату регистрации")
    @NotNull
    private String registryDate;

    @NotNull
    @Min(value = 1, message = "Выберите вид спорта")
    private Integer disciplineId;

    private Integer rankId;

    @NotNull
    private String isNationalTeam;

    @NotNull
    private String isCityTeam;
}
