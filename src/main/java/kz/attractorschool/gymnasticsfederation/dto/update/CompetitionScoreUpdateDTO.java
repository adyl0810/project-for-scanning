package kz.attractorschool.gymnasticsfederation.dto.update;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompetitionScoreUpdateDTO {
    private int score;
//    private int criterionId;
//    private int programId;
//    private int athleteId;
//    private int judgeId;
}
