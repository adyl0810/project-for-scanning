package kz.attractorschool.gymnasticsfederation.dto.update;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class FederationUpdateDTO {
    @Size(min = 5, message = "Ведите полное название федерации")
    @NotNull
    private String name;

    @NotNull
    @Size(min = 5, message = "Ведите ФИО директора")
    private String director;

    @Size(min = 5, message = "Введите полный адрес")
    @NotNull
    private String address;

    @NotNull
    @Pattern(regexp = "^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$", message = "Введите номер телефона в формате +70001112233")
    private String phone;
}
