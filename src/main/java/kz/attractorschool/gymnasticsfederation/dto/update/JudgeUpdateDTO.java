package kz.attractorschool.gymnasticsfederation.dto.update;

import com.sun.istack.NotNull;
import lombok.*;

import javax.validation.constraints.Min;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JudgeUpdateDTO {
    @NotNull
    @Min(value = 1, message = "Выберите вид спорта")
    private Integer disciplineId;

    @NotNull
    @Min(value = 1, message = "Выберите разряд (Доступен вариант 'Нет разряда')")
    private Integer categoryId;
}
