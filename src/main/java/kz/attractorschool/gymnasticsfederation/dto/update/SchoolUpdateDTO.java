package kz.attractorschool.gymnasticsfederation.dto.update;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SchoolUpdateDTO {
    @NotNull
    @Size(min = 5, message = "Введите полное название школы")
    private String name;

    @Column
    @NotNull
    @Size(min = 5, message = "Введите полные данные о директоре")
    private String director;

    @NotNull
    @NotBlank
    @Size(min = 5, message = "Введите полный адрес")
    private String address;

    @NotNull
    @Pattern(regexp = "^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$", message = "Введите номер телефона в формате +70001112233")
    private String phone;

    @NotNull
    @Min(value = 1, message = "Выберите федерацию")
    private Integer federationId;
}
