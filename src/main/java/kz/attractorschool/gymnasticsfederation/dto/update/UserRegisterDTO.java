package kz.attractorschool.gymnasticsfederation.dto.update;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class UserRegisterDTO {
    @NotNull(message = "Введите старый мейл")
    private String oldEmail;

    @NotNull(message = "Введите старый пароль")
    private String oldPassword;

    @NotNull
    @Pattern(regexp = "([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})", message = "Введите корректный мэйл")
    private String email;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$", message = "Пароль должен содержать букву в верхнем регистре, нижнем регистре, цифру, длина - 8 символов")
    private String password;
}
