package kz.attractorschool.gymnasticsfederation.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor
@ResponseStatus(HttpStatus.CONFLICT)
@Getter
public class UserAlreadyExistsException extends RuntimeException {
    private String result = "Мейл занят. Введите другой мейл";
}
