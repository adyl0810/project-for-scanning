use `esdp`;

CREATE Table `users` (
    `id` int auto_increment not null,
    `email` varchar(128) not null,
    `password` varchar(128) not null,
    `active` boolean not null default true,
    `role` varchar(24) not null,
    primary key (`id`),
    unique index `email_unique` (`email` ASC)
);

INSERT INTO `users`(email, password, role) VALUES
('federation_1@test.com', '$2a$10$QZNxMmiTQvVN62YtFfxPP.pKxWhlPICCCQ5bYFn6pjfjnQdg93WkC', 'FEDERATION'),
('school_1@test.com', '$2a$10$QZNxMmiTQvVN62YtFfxPP.pKxWhlPICCCQ5bYFn6pjfjnQdg93WkC', 'SCHOOL'),
('school_2@test.com', '$2a$10$QZNxMmiTQvVN62YtFfxPP.pKxWhlPICCCQ5bYFn6pjfjnQdg93WkC', 'SCHOOL'),
('school_3@test.com', '$2a$10$QZNxMmiTQvVN62YtFfxPP.pKxWhlPICCCQ5bYFn6pjfjnQdg93WkC', 'SCHOOL'),
('admin@test.com', '$2a$10$QZNxMmiTQvVN62YtFfxPP.pKxWhlPICCCQ5bYFn6pjfjnQdg93WkC', 'ADMIN');

create table `federations`(
      `id` int auto_increment not null,
      `name` varchar(128) not null,
      `director` varchar(128) not null ,
      `user_id` int not null references `users`,
      `address` varchar(128) not null,
      `phone` varchar(128) not null,
      `is_del` boolean default false,
      primary key (`id`)
);

create table `schools`(
      `id` int auto_increment not null,
      `name` varchar(128) not null,
      `director` varchar(128) not null ,
      `user_id` int not null references `users`,
      `address` varchar(128) not null,
      `phone` varchar(128) not null,
      `is_del` boolean default false,
      `federation_id` int not null references `federations` (`id`),
      primary key (`id`)
);