use esdp;

INSERT INTO `medical_files` (file_path) VALUES
('P6.01 - ru - 10100533598058.pdf');

INSERT INTO `doping_files` (file_path) VALUES
    ('P6.01 - ru - 10100533598058.pdf');

INSERT INTO `registry_files` (file_path) VALUES
    ('P6.01 - ru - 10100533598058.pdf');

INSERT INTO `rank_files` (file_path) VALUES
    ('P6.01 - ru - 10100533598058.pdf');

INSERT INTO `coach_category_files` (file_path) VALUES
    ('P6.01 - ru - 10100533598058.pdf');

INSERT INTO `judge_category_files` (file_path) VALUES
    ('P6.01 - ru - 10100533598058.pdf');


INSERT INTO `athletes`(person_id, school_id, registry_number, registry_file_id, medical_file_id, doping_file_id, status, registry_date, is_city_team, is_national_team, discipline_id, rank_id, rank_file_id) VALUES
(1, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(2, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(3, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(4, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(5, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(6, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(7, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(8, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(9, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(10, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(11, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(12, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(13, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(14, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(15, 1, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(16, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(17, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(18, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(19, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(20, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(21, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(22, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(23, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(24, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(25, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(26, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(27, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(28, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(29, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(30, 2, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(31, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(32, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(33, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(34, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(35, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(36, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(37, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(38, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(39, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(40, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(41, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(42, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(43, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(44, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1),
(45, 3, 1, 1, 1, 1, 'АКТИВНЫЙ', '2020-01-01', true, true, 2, 1, 1);


INSERT INTO `coaches`(person_id, registry_number, school_id, discipline_id, category_id, category_file_id, doping_file_id, is_del) VALUES
(1, 1, 1, 1, 1, 1, 1, false),
(2, 1, 1, 1, 1, 1, 1,false),
(3, 1, 1, 1, 1, 1, 1, false),
(16, 1, 2, 1, 1, 1, 1, false),
(17, 1, 2, 1, 1, 1, 1, false),
(18, 1, 2, 1, 1, 1, 1, false),
(44, 1, 3, 2, 1, 1, 1, false),
(43, 1, 3, 3, 1, 1, 1, false),
(42, 1, 3, 4, 1, 1, 1, false);

INSERT INTO `judges` (person_id, registry_number, school_id, discipline_id, category_id, category_file_id) VALUES
(4, 1, 1, 1, 1, 1),
(5, 1, 1, 1, 1, 1),
(6, 1, 1, 1, 2, 1),
(19, 1, 2, 1, 4, 1),
(22, 1, 2, 1, 3, 1),
(23, 1, 2, 1, 2, 1),
(40, 1, 3, 1, 4, 1),
(39, 1, 3, 1, 3, 1),
(38, 1, 3, 1, 2, 1);