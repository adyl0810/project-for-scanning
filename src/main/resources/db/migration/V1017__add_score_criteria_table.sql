use esdp;

CREATE TABLE `score_criteria`(
    `id` int auto_increment not null,
    `name` varchar(100) not null,
    `min_score` int,
    `max_score` int,
    `is_del` boolean not null default false,
    `discipline_id` int not null references `disciplines`(`id`),
    `discipline_type_id` int references `discipline_types`(`id`),
    PRIMARY KEY (`id`)
);