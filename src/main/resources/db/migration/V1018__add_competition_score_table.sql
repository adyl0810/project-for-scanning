use esdp;

CREATE TABLE `competition_scores`(
    `id` int auto_increment not null,
    `score` double not null,
    `status` varchar(100),
#     `deduction` double,
#     `sum` double,
#     `place` int,
    `criterion_id` int not null references `score_criteria`(`id`),
    `discipline_type_id` int not null references `competitions_discipline` (`id`),
    `program_id` int not null references `competitions_discipline_programs`(`id`),
    `age_category_id` int not null references `competitions_discipline_ages`(`id`),
    `athlete_id` int not null references `participation_applications_athletes`(`athlete_id`),
#     `judge_id` int not null references `participation_applications_judges`(`judge_id`),
    PRIMARY KEY (`id`)
);