use esdp;

CREATE TABLE `school_changes`(
    `id` int auto_increment not null,
    `date` date not null,
    `status` varchar(100) not null,
    `old_school_id` int not null references `schools`(`id`),
    `new_school_id` int not null references `schools`(`id`),
    `athlete_id` int references `athletes`(`id`),
    `judge_id` int references `judges`(`id`),
    `coach_id` int references `coaches`(`id`),
    PRIMARY KEY (`id`)
);