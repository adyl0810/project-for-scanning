use esdp;

CREATE TABLE `competition_orders`(
    `id` int not null auto_increment,
    `athlete_id` int REFERENCES `athletes`(`id`),
    `athletes_team_id` int REFERENCES `athletes_teams`(`id`),
    `competition_id` INT NOT NULL REFERENCES `competitions`(`id`),
    `discipline_id`     int                not null references `discipline_types` (`id`),
    `age_category_id` int not null references `age_categories`(`id`),
    `flow` int not null,
    `number` int not null,
    PRIMARY KEY (`id`)
)