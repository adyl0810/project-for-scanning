use esdp;

CREATE TABLE `athletes_teams`(
    `id` int not null auto_increment,
    `number` int not null,
    `school_id` int not null references `schools`(`id`),
    `application_id` int not null references `participation_applications`(`id`),
    `competition_id` int not null references `competitions`(`id`),
    `discipline_type_id` int not null references `discipline_types`(`id`),
    `age_category_id` int not null references `age_categories`(`id`),
    primary key (`id`)
);