use esdp;

CREATE TABLE `secretaries`(
    `id` int not null auto_increment,
    `name` varchar(100) not null,
    `surname` varchar(100) not null,
    `user_id` int not null references `users`(`id`),
    `school_id` int not null references `schools`(`id`),
    `is_del` boolean not null,
    primary key (`id`)
);