USE `esdp`;

CREATE TABLE `competition_score_sums`
(
    `id`                 int    not null auto_increment,
    `sum`                double not null,
    `discipline_type_id` int    not null references `competitions_discipline` (`id`),
    `program_id`         int    not null references `competitions_discipline_programs` (`id`),
    `age_category_id`    int    not null references `competitions_discipline_ages` (`id`),
    `athlete_id`         int    not null references `participation_applications_athletes` (`athlete_id`),
    primary key (`id`)
)