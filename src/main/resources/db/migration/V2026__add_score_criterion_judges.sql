USE `esdp`;

CREATE TABLE `score_criterion_judges`
(
    `id`           int not null auto_increment,
    `judge_id`     int not null references `participation_applications_judges` (id),
    `criterion_id` int not null references `score_criteria` (id),
    primary key (`id`)
)