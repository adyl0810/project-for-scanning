$(function () {
    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

$('#discipline').change(function () {
    let disciplineId = $(this).val()
    $.ajax({
        url: `/api/competition/disciplines/types/${disciplineId}`,
        type: "GET",
        success: function (disciplineTypes) {
            $("#disciplineType").empty()
            $("#disciplineType").append(`<option selected value="0">Выберите дисциплину</option>`)
            for (let i = 0; i < disciplineTypes.length; i++) {
                $("#disciplineType").append(`<option value="${disciplineTypes[i].id}">${disciplineTypes[i].name}</option>`)
            }
        }
    })
})

$('#disciplineType').change(function () {
    let disciplineTypeId = $(this).val()
    $.ajax({
        url: `/api/competition/disciplines/programs/${disciplineTypeId}`,
        type: "GET",
        success: function (competitionPrograms) {
            $('#competitionProgramDiv').empty()
            $("#competitionProgramDiv").append(`<option selected>Выберите соревновательную программу</option>`)
            for (let i = 0; i < competitionPrograms.length; i++) {
                $("#competitionProgramDiv").append(`<option value="${competitionPrograms[i].id}">${competitionPrograms[i].name}</option>`)
            }
        }
    })
})
