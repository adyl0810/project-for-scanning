let disciplineTypeDiv = $('.discipline-div')
for (let i = 0; i < disciplineTypeDiv.length; i++) {
    let disciplineTypeId = $(disciplineTypeDiv).eq(i).find('.discipline-id').val()
    let ageCategoryDiv = $(disciplineTypeDiv).eq(i).find('.age-category-div')
    for (let j = 0; j < ageCategoryDiv.length; j++) {
        let ageCategoryId = $(ageCategoryDiv).eq(j).find('.age-category-id').val()
        let athleteDiv = $(ageCategoryDiv).eq(j).find('.athlete-tr')
        for (let k = 0; k < athleteDiv.length; k++) {
            let athleteId = $(athleteDiv).eq(k).find('.athlete-id').val()
            let programDiv = $(athleteDiv).eq(k).find('.program-div')
            for (let l = 0; l < programDiv.length; l++) {
                let programId = $(programDiv).eq(l).find('.program-id').val()
                let programScore = $(programDiv).eq(l).find('.score-sum')
                $.ajax({
                    url : '/api/score/sum',
                    type : 'GET',
                    data : {
                        disciplineTypeId : disciplineTypeId,
                        programId : programId,
                        ageCategoryId : ageCategoryId,
                        athleteId : athleteId
                    },
                    success : function (result) {
                        programScore.text(result.sum)
                    }
                })
            }
        }
    }
}
$.ajax()