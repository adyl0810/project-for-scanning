$(function () {
    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

$(".toggle-btn").click(function () {
    $(this).closest('.toggle-parent-div').children('.toggle-div').toggle(500)
})

function removeMinAndMaxVal(arr) {
    arr.sort()
    arr.pop()
    arr.shift()
    return arr
}

function getScores() {
    let disciplineDiv = $('.discipline-div')
    for (let i = 0; i < disciplineDiv.length; i++) {
        let disciplineId = $(disciplineDiv).eq(i).find('.discipline-id').val()
        let ageCategoryDiv = $(disciplineDiv).eq(i).find('.age-category-div')
        for (let j = 0; j < ageCategoryDiv.length; j++) {
            let ageCategoryId = $(ageCategoryDiv).eq(j).find('.age-category-id').val()
            let programDiv = $(ageCategoryDiv).eq(j).find('.program-div')
            for (let k = 0; k < programDiv.length; k++) {
                let programId = $(programDiv).eq(k).find('.program-id').val()
                let athleteDiv = $(programDiv).eq(k).find('.athlete-tr')
                for (let l = 0; l < athleteDiv.length; l++) {
                    let arrayD = []
                    let arrayE = []
                    let arrayA = []
                    let deduction = 0
                    let sumD = 0
                    let sumE = 0
                    let sumA = 0
                    let sum = 0
                    let athleteId = $(athleteDiv).eq(l).find('.athlete-id').val()
                    let criterionDiv = $(athleteDiv).eq(l).find('.criterion-td')
                    for (let m = 0; m < criterionDiv.length; m++) {
                        let criterionId = $(criterionDiv).eq(m).find('.criterion-id').val()
                        let $criterionVal = $(criterionDiv).eq(m).find('.criterion-score').val()
                        $.ajax({
                            url: '/api/score',
                            type: 'GET',
                            data: {
                                ageCategoryId: ageCategoryId,
                                athleteId: athleteId,
                                programId: programId,
                                criterionId: criterionId,
                                disciplineTypeId: disciplineId
                            },
                            success: function (result) {
                                if ($.trim(result)) {
                                    $(criterionDiv).eq(m).find('.criterion-value').text(result.score)
                                    $(criterionDiv).eq(m).find('.criterion-score').attr('value', result.score)
                                    $(criterionDiv).eq(m).find('.criterion-score').prop('hidden', true)
                                    $(athleteDiv).eq(l).find('.save-athlete-score-btn').prop('hidden', true)
                                    $(athleteDiv).eq(l).find('.edit-athlete-score-btn').prop('hidden', false)
                                    if (m === 0 || m === 1) {
                                        arrayD.push(result.score)
                                    }
                                    if (m === 2 || m === 3 || m === 4 || m === 5) {
                                        arrayE.push(result.score)
                                    }
                                    if (m === 6 || m === 7 || m === 8 || m === 9) {
                                        arrayA.push(result.score)
                                    }
                                    if (m === 10) {
                                        deduction = result.score
                                        for (let n = 0; n < arrayD.length; n++) {
                                            sumD += parseFloat(arrayD[n]) || 0
                                        }
                                        arrayE = removeMinAndMaxVal(arrayE)
                                        for (let n = 0; n < arrayE.length; n++) {
                                            sumE += parseFloat(arrayE[n]) || 0
                                        }
                                        sumE = 10 - (sumE / 2)
                                        arrayA = removeMinAndMaxVal(arrayA)
                                        for (let n = 0; n < arrayA.length; n++) {
                                            sumA += parseFloat(arrayA[n]) || 0
                                        }
                                        sumA = 10 - (sumA / 2)
                                        sum = (sumD + sumE + sumA - deduction).toFixed(2)
                                        $(athleteDiv).eq(l).find('.score-total').text(sum)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    let sums = $('.score-total')
    let isScoresEmpty = true
    const saveAllScoresBtn = $('#save-all-results')
    for (let i = 0; i < sums.length; i++) {
        isScoresEmpty = $(sums).eq(i).text() === "";
        if (isScoresEmpty === true) {
            break
        }
    }
    if (isScoresEmpty === true) {
        saveAllScoresBtn.prop('disabled', true)
    } else {
        saveAllScoresBtn.removeAttr('disabled')
    }
}

getScores()

$('#save-judges').on('click', function () {
    $('.judge-criterion-div').each(function () {
        let selectJudgeDiv = $(this).find('.select-judge')
        let judgeInputDiv = $(this).find('.judge-input-div')
        let criterionId = $(this).find('.judge-criterion-id').val()
        let judgeId = $(this).find('.select-judge').val()
        $.ajax({
            url : '/api/score/judge',
            type : 'POST',
            data : {
                judgeId : judgeId,
                criterionId : criterionId
            },
            success : function (result) {
                console.log(result)
                selectJudgeDiv.attr('hidden', true)
                judgeInputDiv.text(result.judge.judge.person.surname + " " + result.judge.judge.person.name)
            }
        })
    })
})


$(document).ready(function () {
    $('.input-td input[type="number"]').on('keyup', function () {
        let empty = false

        $(this).closest('.athlete-tr').find('.input-td input[type="number"]').each(function () {
            empty = $(this).val().length === 0
        })

        if (empty) {
            $(this).closest('.athlete-tr').find('.save-athlete-score-btn').attr('disabled', 'disabled')
        } else {
            $(this).closest('.athlete-tr').find('.save-athlete-score-btn').attr('disabled', false)
        }
    })
})

$(".save-athlete-score-btn").click(function () {
    let disciplineTypeId = $(this).closest('.discipline-div').find('.discipline-id').val()
    let ageCategoryId = $(this).closest('.age-category-div').find('.age-category-id').val()
    let programId = $(this).closest('.program-div').find('.program-id').val()
    let athleteId = $(this).closest('.athlete-tr').find('.athlete-id').val()

    let criteriaArray = $(this).closest('.athlete-tr').find('.criterion-td')
    let arrayD = [
        $(criteriaArray).eq(0).find('.criterion-score').val(),
        $(criteriaArray).eq(1).find('.criterion-score').val()
    ]
    let arrayE = [
        $(criteriaArray).eq(2).find('.criterion-score').val(),
        $(criteriaArray).eq(3).find('.criterion-score').val(),
        $(criteriaArray).eq(4).find('.criterion-score').val(),
        $(criteriaArray).eq(5).find('.criterion-score').val()
    ]
    arrayE = removeMinAndMaxVal(arrayE)
    let arrayA = [
        $(criteriaArray).eq(6).find('.criterion-score').val(),
        $(criteriaArray).eq(7).find('.criterion-score').val(),
        $(criteriaArray).eq(8).find('.criterion-score').val(),
        $(criteriaArray).eq(9).find('.criterion-score').val()
    ]
    arrayA = removeMinAndMaxVal(arrayA)
    let deduction = $(criteriaArray).eq(10).find('.criterion-score').val()
    let sumD = 0
    let sumE = 0
    let sumA = 0

    for (let i = 0; i < arrayD.length; i++) {
        sumD += parseFloat(arrayD[i]) || 0
    }

    for (let i = 0; i < arrayE.length; i++) {
        sumE += parseFloat(arrayE[i]) || 0
    }

    for (let i = 0; i < arrayA.length; i++) {
        sumA += parseFloat(arrayA[i]) || 0
    }
    sumE = 10 - sumE / 2
    sumA = 10 - sumA / 2
    let sum = sumD + sumE + sumA - deduction

    for (let i = 0; i < criteriaArray.length; i++) {
        let criteriaVal = $(criteriaArray).eq(i).find('.criterion-score').val()
        let criteriaId = $(criteriaArray).eq(i).find('.criterion-id').val()

        $.ajax({
            url: '/api/score',
            type: 'POST',
            data: {
                score: criteriaVal,
                criterionId: criteriaId,
                disciplineTypeId: disciplineTypeId,
                programId: programId,
                ageCategoryId: ageCategoryId,
                athleteId: athleteId
            },
            success: function (competitionScore) {
                let scoreId = competitionScore
                $(criteriaArray).eq(i).find('.criterion-score').prop('hidden', true)
                $(criteriaArray).eq(i).find('.criterion-value').text(`${criteriaVal}`)
                $(criteriaArray).eq(i).append(`<input type="hidden" value="${scoreId}" class="score-id">`)
            }
        })
    }

    $(this).closest('.athlete-tr').find('.score-total').text(sum)
    $(this).prop('hidden', true)
    $(this).closest('.save-and-edit-btn-td').find('.edit-athlete-score-btn').prop('hidden', false)
    let athleteScoreArr = $(this).closest('.athletes-table-body').find('.score-total')
    let isScoreEmpty = true
    for (let i = 0; i < athleteScoreArr.length; i++) {
        isScoreEmpty = $(athleteScoreArr).eq(i).text() === "";
    }
    if (isScoreEmpty === true) {
        $(this).closest('.athletes-table-body').find('.sort-places').prop('disabled', true)
    } else {
        $(this).closest('.program-div').find('.sort-places').removeAttr('disabled')
    }
    let sums = $('.score-total')
    let isScoresEmpty = true
    const saveAllScoresBtn = $('#save-all-results')
    for (let i = 0; i < sums.length; i++) {
        isScoresEmpty = $(sums).eq(i).text() === "";
        if (isScoresEmpty === true) {
            break
        }
    }
    if (isScoresEmpty === true) {
        saveAllScoresBtn.prop('disabled', true)
    } else {
        saveAllScoresBtn.removeAttr('disabled')
    }
})

$(".save-edited-athlete-score-btn").on('click', function () {

    let criteriaArray = $(this).closest('.athlete-tr').find('.criterion-td')
    let arrayD = [
        $(criteriaArray).eq(0).find('.criterion-score').val(),
        $(criteriaArray).eq(1).find('.criterion-score').val()
    ]
    let arrayE = [
        $(criteriaArray).eq(2).find('.criterion-score').val(),
        $(criteriaArray).eq(3).find('.criterion-score').val(),
        $(criteriaArray).eq(4).find('.criterion-score').val(),
        $(criteriaArray).eq(5).find('.criterion-score').val()
    ]
    arrayE = removeMinAndMaxVal(arrayE)
    let arrayA = [
        $(criteriaArray).eq(6).find('.criterion-score').val(),
        $(criteriaArray).eq(7).find('.criterion-score').val(),
        $(criteriaArray).eq(8).find('.criterion-score').val(),
        $(criteriaArray).eq(9).find('.criterion-score').val()
    ]
    arrayA = removeMinAndMaxVal(arrayA)
    let deduction = $(criteriaArray).eq(10).find('.criterion-score').val()
    let sumD = 0
    let sumE = 0
    let sumA = 0

    for (let i = 0; i < arrayD.length; i++) {
        sumD += parseFloat(arrayD[i]) || 0
    }

    for (let i = 0; i < arrayE.length; i++) {
        sumE += parseFloat(arrayE[i]) || 0
    }

    for (let i = 0; i < arrayA.length; i++) {
        sumA += parseFloat(arrayA[i]) || 0
    }
    sumE = 10 - sumE / 2
    sumA = 10 - sumA / 2
    let sum = sumD + sumE + sumA - deduction

    for (let i = 0; i < criteriaArray.length; i++) {
        let criteriaVal = $(criteriaArray).eq(i).find('.criterion-score').val()
        let criteriaId = $(criteriaArray).eq(i).find('.criterion-id').val()
        let scoreId = $(criteriaArray).eq(i).find('.score-id').val()
        console.log(scoreId)

        $.ajax({
            url: `/api/score/${scoreId}`,
            type: 'PUT',
            data: {
                score: criteriaVal,
            },
            success: function () {
                $(criteriaArray).eq(i).find('.criterion-score').prop('hidden', true)
                $(criteriaArray).eq(i).find('.criterion-value').text(`${criteriaVal}`)
            }
        })
        $(criteriaArray).eq(i).find('.criterion-value').text(`${criteriaVal}`)
        $(criteriaArray).eq(i).find('.criterion-score').prop('hidden', true)
    }

    $(this).closest('.athlete-tr').find('.score-total').text(sum)
    $(this).prop('hidden', true)
    $(this).closest('.save-and-edit-btn-td').find('.edit-athlete-score-btn').prop('hidden', false)
    let athleteScoreArr = $(this).closest('.athletes-table-body').find('.score-total')
    let scoreEmpty = true
    for (let i = 0; i < athleteScoreArr.length; i++) {
        scoreEmpty = $(athleteScoreArr).eq(i).text() === "";
    }
    if (scoreEmpty === true) {
        $(this).closest('.athletes-table-body').find('.sort-places').prop('disabled', true)
    } else {
        $(this).closest('.program-div').find('.sort-places').removeAttr('disabled')
    }

    let sums = $('.score-total')
    let isScoresEmpty = true
    const saveAllScoresBtn = $('#save-all-results')
    for (let i = 0; i < sums.length; i++) {
        isScoresEmpty = $(sums).eq(i).text() === "";
        if (isScoresEmpty === true) {
            break
        }
    }
    if (isScoresEmpty === true) {
        saveAllScoresBtn.prop('disabled', true)
    } else {
        saveAllScoresBtn.removeAttr('disabled')
    }
})

$(".edit-athlete-score-btn").click(function () {
    let criteriaArray = $(this).closest('.athlete-tr').find('.criterion-td')
    for (let i = 0; i < criteriaArray.length; i++) {
        $(criteriaArray).eq(i).find('.criterion-score').removeAttr('hidden')
        $(criteriaArray).eq(i).find('.criterion-value').text("")
    }
    $(this).prop('hidden', true)
    $(this).closest('.save-and-edit-btn-td').find('.save-edited-athlete-score-btn').prop('hidden', false)
    $(this).closest('.athlete-tr').find('.score-total').text("")
    const saveAllScoresBtn = $('#save-all-results')
    saveAllScoresBtn.prop('disabled', true)
})

$('.sort-places').on('click', function () {
    let divArr = $(this).closest('.program-div').find('.athlete-tr')
    divArr.sort(function (a, b) {
        return $(a).find(".score-total").text() > $(b).find(".score-total").text() ? -1 : 1;
    })
    $(this).closest('.program-div').find('.athletes-table-body').append(divArr)
    for (let i = 0; i < divArr.length; i++) {
        let place = i + 1
        $(divArr).eq(i).find('.athlete-place').text(`${place}`)
    }
})

$('#save-all-results').on('click', function () {
    let scoreDiv = $('.score-total')
    for (let i = 0; i < scoreDiv.length; i++) {
        let sum = parseFloat($(scoreDiv).eq(i).text())
        console.log(sum)
        let athleteId = $(scoreDiv).eq(i).closest('.athlete-tr').find('.athlete-id').val()
        let programId = $(scoreDiv).eq(i).closest('.program-div').find('.program-id').val()
        let ageCategoryId = $(scoreDiv).eq(i).closest('.age-category-div').find('.age-category-id').val()
        let disciplineTypeId = $(scoreDiv).eq(i).closest('.discipline-div').find('.discipline-id').val()
        $.ajax({
            url : '/api/score/sum',
            type : 'POST',
            data : {
                sum : sum,
                disciplineTypeId : disciplineTypeId,
                programId : programId,
                ageCategoryId : ageCategoryId,
                athleteId : athleteId
            }
        })
    }
    let competitionId = $('#competition-id').val()
    window.location.replace(`/score/${competitionId}/final`)
})