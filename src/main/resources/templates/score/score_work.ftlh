<#--Код для вставки основного шаблона в страницу-->
<#import "../partials/main.ftlh" as main/>

<@main.renderWith title="Рабочий протокол" scripts=["js/score_work.js"]>
    <div class="container">
        <div class="card my-3">
            <div class="card-body">
                <h3>${competition.discipline.name}</h3>
                <div class="row">
                    <div class="col-3 fs-6">
                        Наименование:
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.name}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-3 fs-6">
                        Даты проведения:
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.startDate} - ${competition.finishDate}
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 fs-6">
                        Крайний срок подачи заявки
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.participationDate}
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 fs-6">
                        Уровень
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.level}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-3 fs-6">
                        Место проведения:
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.country}, ${competition.city}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-3 fs-6">
                        Адрес:
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.address}, ${competition.areaName}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-3 fs-6">
                        Контактное лицо:
                    </div>
                    <div class="col-9 fs-6">
                        ${competition.contact} (${competition.phone})
                    </div>
                </div>
            </div>
        </div>

        <div class="card my-3">
            <div class="card-body">
                <div class="judge-div toggle-parent-div">
                    <div class="row">
                        <div class="col">
                            <h5>Распределение судей</h5>
                        </div>
                        <div class="col text-end">
                            <button class="toggle-btn btn btn-primary btn-sm">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-arrows-collapse" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                          d="M1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8zm7-8a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5 4.293V.5A.5.5 0 0 1 8 0zm-.5 11.707-1.146 1.147a.5.5 0 0 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 11.707V15.5a.5.5 0 0 1-1 0v-3.793z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="toggle-div">
                        <#list criteria as criterion>
                            <div class="row judge-criterion-div my-2">
                                <div class="col">
                                    ${criterion.name}
                                    <input type="hidden" value="${criterion.id}" class="judge-criterion-id">
                                </div>
                                <div class="col judge-input-div">
                                    <select class="form-select select-judge">
                                        <option value="">Выберите судью</option>
                                        <#list judges as judge>
                                            <option value="${judge.id}">${judge.judge.person.surname} ${judge.judge.person.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                            <hr>
                        </#list>
                        <button id="save-judges" class="btn btn-primary">Сохранить судей</button>
                    </div>
                </div>
            </div>
        </div>

        <#list disciplineTypes as disciplineType>
            <div class="card my-3">
                <div class="card-body">
                    <div class="discipline-div toggle-parent-div my-3">
                        <input type="hidden" value="${disciplineType.id}" class="discipline-id">
                        <div class="row">
                            <div class="col"><h4><b>${disciplineType.discipline.name}</b></h4></div>
                            <div class="col text-end">
                                <button class="toggle-btn btn btn-primary btn-sm">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-arrows-collapse" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                              d="M1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8zm7-8a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5 4.293V.5A.5.5 0 0 1 8 0zm-.5 11.707-1.146 1.147a.5.5 0 0 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 11.707V15.5a.5.5 0 0 1-1 0v-3.793z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <#list ageCategories as a>
                            <#if a.discipline.id = disciplineType.discipline.id>
                                <div class="toggle-div toggle-parent-div age-category-div my-3">
                                    <div class="row">
                                        <div class="col">
                                            <h5>
                                                <#if a.ageCategory.maxYear?? && a.ageCategory.minYear?? && a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">#{a.ageCategory.maxYear} - #{a.ageCategory.minYear}
                                                    : ${a.ageCategory.rank.name}
                                                <#elseif a.ageCategory.maxYear?? && !a.ageCategory.minYear?? && a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">Младше #{a.ageCategory.maxYear} : ${a.ageCategory.rank.name}
                                                <#elseif a.ageCategory.maxYear?? && a.ageCategory.minYear?? && !a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">#{a.ageCategory.maxYear} - #{a.ageCategory.minYear}
                                                <#elseif !a.ageCategory.maxYear?? && a.ageCategory.minYear?? && a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">Старше #{a.ageCategory.minYear} : ${a.ageCategory.rank.name}
                                                <#elseif !a.ageCategory.maxYear?? && a.ageCategory.minYear?? && !a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">Старше #{a.ageCategory.minYear}
                                                <#elseif a.ageCategory.maxYear?? && !a.ageCategory.minYear?? && !a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">Младше #{a.ageCategory.maxYear}
                                                <#elseif !a.ageCategory.maxYear?? !&& a.ageCategory.minYear?? && a.ageCategory.rank??>
                                                    <input type="hidden" class="age-category-id"
                                                           value="${a.id}">${a.ageCategory.rank.name}
                                                </#if>
                                            </h5>
                                        </div>
                                        <div class="col text-end">
                                            <button class="toggle-btn btn btn-primary btn-sm">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor"
                                                     class="bi bi-arrows-collapse" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd"
                                                          d="M1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8zm7-8a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5 4.293V.5A.5.5 0 0 1 8 0zm-.5 11.707-1.146 1.147a.5.5 0 0 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 11.707V15.5a.5.5 0 0 1-1 0v-3.793z"/>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                    <hr>
                                    <#list programs as program>
                                        <#if program.discipline.id = disciplineType.discipline.id>
                                            <div class="toggle-div toggle-parent-div my-3 program-div">
                                                <input type="hidden" class="program-id" value="${program.id}">
                                                <div class="row">
                                                    <div class="col"><p class="my-2">
                                                            <b>${program.competitionProgram.name}</b></p></div>
                                                    <div class="col text-end">
                                                        <button class="toggle-btn btn btn-primary btn-sm">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                 height="16" fill="currentColor"
                                                                 class="bi bi-arrows-collapse" viewBox="0 0 16 16">
                                                                <path fill-rule="evenodd"
                                                                      d="M1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8zm7-8a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5 4.293V.5A.5.5 0 0 1 8 0zm-.5 11.707-1.146 1.147a.5.5 0 0 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 11.707V15.5a.5.5 0 0 1-1 0v-3.793z"/>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </div>
                                                <table class="table table-striped table-bordered table-hover text-center toggle-div">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Участник</th>
                                                        <th scope="col">Год рождения</th>
                                                        <th scope="col">Город</th>
                                                        <#list criteria as criterion>
                                                            <th scope="col">${criterion.name}</th>
                                                        </#list>
                                                        <th scope="col">Сумма</th>
                                                        <th scope="col">Место</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="athletes-table-body">
                                                    <#if (a.maxTeams??) && (a.discipline.participantsAmountMax??)>
                                                        <#assign x=a.maxTeams>
                                                        <#list 1..x as i>
                                                            <tr class="athlete-tr">
                                                                <th>Команда №${i}</th>
                                                                <td>-</td>
                                                                <td>-</td>
                                                                <#list criteria as criterion>
                                                                    <td class="criterion-td">
                                                                        <input type="number"
                                                                               class="form-control criterion-score">
                                                                        <input type="hidden"
                                                                               class="criterion-id"
                                                                               value="${criterion.id}">
                                                                        <div class="criterion-value"></div>
                                                                    </td>
                                                                </#list>
                                                                <td class="score-total"></td>
                                                                <td class="athlete-place"></td>

                                                                <#if user?? && (user.role == "ADMIN" || user.role == "SECRETARY")>
                                                                    <td class="save-and-edit-btn-td">
                                                                        <button class="btn btn-primary save-athlete-score-btn"
                                                                                disabled>
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                 width="16" height="16"
                                                                                 fill="currentColor"
                                                                                 class="bi bi-save"
                                                                                 viewBox="0 0 16 16">
                                                                                <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"/>
                                                                            </svg>
                                                                        </button>
                                                                        <button class="btn btn-primary edit-athlete-score-btn"
                                                                                hidden>
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                 width="16" height="16"
                                                                                 fill="currentColor"
                                                                                 class="bi bi-pencil"
                                                                                 viewBox="0 0 16 16">
                                                                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                                                            </svg>
                                                                        </button>
                                                                    </td>
                                                                </#if>

                                                            </tr>
                                                        </#list>
                                                    <#elseif a.maxAthletes??>
                                                        <#list applications as application>
                                                            <#list application.applicationAthletes as athlete>
                                                                <#if athlete.disciplineAge.id = a.id && athlete.disciplineType.id = disciplineType.discipline.id>
                                                                    <tr class="athlete-tr">
                                                                        <input type="hidden" class="athlete-id"
                                                                               value="${athlete.id}">
                                                                        <th scope="row">${athlete.athlete.person.surname} ${athlete.athlete.person.name}</th>
                                                                        <td>#{athlete.athlete.person.birthday.getYear()}</td>
                                                                        <td>${athlete.athlete.person.city}</td>
                                                                        <#if user?? && (user.role == "ADMIN" || user.role == "SECRETARY")>
                                                                            <#list criteria as criterion>
                                                                                <td class="criterion-td input-td">
                                                                                    <input type="number"
                                                                                           class="form-control criterion-score">
                                                                                    <input type="hidden"
                                                                                           class="criterion-id"
                                                                                           value="${criterion.id}">
                                                                                    <div class="criterion-value"></div>
                                                                                </td>
                                                                            </#list>
                                                                        <#else>
                                                                            <#list criteria as criterion>
                                                                                <td class="criterion-td input-td">
                                                                                    <#--                                                                                    <input type="number"-->
                                                                                    <#--                                                                                           class="form-control criterion-score">-->
                                                                                    <input type="hidden"
                                                                                           class="criterion-id"
                                                                                           value="${criterion.id}">
                                                                                    <div class="criterion-value"></div>
                                                                                </td>
                                                                            </#list>
                                                                        </#if>
                                                                        <td class="score-total"></td>
                                                                        <td class="athlete-place"></td>

                                                                        <#if user?? && (user.role == "ADMIN" || user.role == "SECRETARY")>
                                                                            <td class="save-and-edit-btn-td">
                                                                                <button class="btn btn-primary save-athlete-score-btn"
                                                                                        disabled>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="16" height="16"
                                                                                         fill="currentColor"
                                                                                         class="bi bi-save"
                                                                                         viewBox="0 0 16 16">
                                                                                        <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"/>
                                                                                    </svg>
                                                                                </button>
                                                                                <button class="btn btn-primary save-edited-athlete-score-btn"
                                                                                        hidden>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="16" height="16"
                                                                                         fill="currentColor"
                                                                                         class="bi bi-save"
                                                                                         viewBox="0 0 16 16">
                                                                                        <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"/>
                                                                                    </svg>
                                                                                </button>
                                                                                <button class="btn btn-primary edit-athlete-score-btn"
                                                                                        hidden>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="16" height="16"
                                                                                         fill="currentColor"
                                                                                         class="bi bi-pencil"
                                                                                         viewBox="0 0 16 16">
                                                                                        <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                                                                    </svg>
                                                                                </button>
                                                                            </td>
                                                                        </#if>

                                                                    </tr>
                                                                </#if>
                                                            </#list>
                                                        </#list>
                                                    </#if>
                                                    </tbody>
                                                </table>
                                                <#if user?? && (user.role == "ADMIN" || user.role == "SECRETARY")>
                                                    <button class="btn btn-primary sort-places" disabled>Распределить
                                                        места
                                                    </button>
                                                </#if>
                                            </div>
                                        </#if>
                                    </#list>
                                </div>
                            </#if>
                        </#list>
                    </div>
                </div>
            </div>
        </#list>
        <input type="hidden" value="${competition.id}" id="competition-id">
        <#if user?? && (user.role == "ADMIN" || user.role == "SECRETARY")>
            <button class="btn btn-primary" id="save-all-results" disabled>Сохранить результаты</button>
        </#if>
        <button onclick="topFunction()" id="scroll-btn" class="btn btn-primary" title="Go to top">Наверх</button>
    </div>

</@main.renderWith>