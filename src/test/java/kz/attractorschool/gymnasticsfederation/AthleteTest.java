//package kz.attractorschool.gymnasticsfederation;
//
//import kz.attractorschool.gymnasticsfederation.common_service.AthleteService;
//import lombok.RequiredArgsConstructor;
//import org.junit.jupiter.api.MethodOrderer;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import static org.junit.Assert.assertEquals;
//
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//@RequiredArgsConstructor
//public class AthleteTest {
//    private WebDriver driver;
//    private String url;
//    private String personUrl;
//    private String personId;
//
//    @BeforeTest
//    public void setProperty() throws InterruptedException {
//        Thread.sleep(1500);
//        System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver\\chromedriver.exe");
//        this.driver = new ChromeDriver();
//        Thread.sleep(1500);
//        System.out.println("Аутентификация");
//        this.driver.get("http://162.55.182.49:8001/login");
//        driver.findElement(By.name("username")).sendKeys("school_1@test.com");
//        driver.findElement(By.name("password")).sendKeys("12345");
//        driver.findElement(By.id("btn")).click();
//    }
//
//    @AfterTest
//    public void quit(){
//        System.out.println("Тест окончен");
//        this.driver.quit();
//    }
//
//    @Order(1)
//    @Test
//    public void aTest() throws InterruptedException {
//        System.out.println("Добавление персоны");
//        this.driver.get("http://162.55.182.49:8001/person");
//        Thread.sleep(2000);
//        assertEquals("Добавить персону", driver.getTitle());
//        driver.findElement(By.name("name")).sendKeys("nameTest");
//        driver.findElement(By.name("surname")).sendKeys("surnameTest");
//        driver.findElement(By.name("surname")).sendKeys("middleNameTest");
//        driver.findElement(By.name("birthday")).sendKeys("2004-05-09");
//        driver.findElement(By.name("iin")).sendKeys("000000000009");
//        driver.findElement(By.name("address")).sendKeys("addressTest");
//        driver.findElement(By.name("city")).sendKeys("cityTest");
//        driver.findElement(By.name("phone")).sendKeys("phoneTest");
//        driver.findElement(By.name("email")).sendKeys("emailTest@test.ru");
//        driver.findElement(By.name("education")).sendKeys("educationTest");
//        driver.findElement(By.name("file")).sendKeys("D:\\ic-user.png");
//        Thread.sleep(2000);
//        Select select = new Select(driver.findElement(By.name("gender")));
//        select.selectByValue("Мужской");
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,300)");
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.id("btn"));
//        btn.click();
//        Thread.sleep(2000);
//        this.personUrl = driver.getCurrentUrl();
//        this.personId = this.personUrl.split("/")[4];
//    }
//
//    @Order(2)
//    @Test
//    public void bTest() throws InterruptedException {
//        System.out.println("Добавление спортсмена");
//        this.driver.get("http://162.55.182.49:8001/athlete");
//        Thread.sleep(2000);
//        assertEquals("Добавить спортсмена", driver.getTitle());
//        Select person = new Select(driver.findElement(By.name("personId")));
//        person.selectByValue(this.personId);
//        Select discipline = new Select(driver.findElement(By.name("disciplineId")));
//        discipline.selectByIndex(1);
//        Select rank = new Select(driver.findElement(By.name("rankId")));
//        rank.selectByIndex(1);
//        driver.findElement(By.name("registryDate")).sendKeys("2020-05-05");
//        driver.findElement(By.name("registryFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        driver.findElement(By.name("rankFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        driver.findElement(By.name("medicalFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        driver.findElement(By.name("dopingFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        Select isNationalTeam = new Select(driver.findElement(By.name("isNationalTeam")));
//        isNationalTeam.selectByIndex(1);
//        Select isCityTeam = new Select(driver.findElement(By.name("isCityTeam")));
//        isCityTeam.selectByIndex(1);
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,300)");
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.id("btn"));
//        btn.click();
//        Thread.sleep(1000);
//        this.url = driver.getCurrentUrl();
//        System.out.println(url);
//    }
//
//    @Order(3)
//    @Test
//    public void cTest() throws InterruptedException {
//        System.out.println("Изменение спортсмена");
//        this.driver.get(this.url + "/update");
//        Thread.sleep(2000);
//        System.out.println("Изменить вид спорта");
//        Select discipline = new Select(driver.findElement(By.name("disciplineId")));
//        discipline.selectByIndex(2);
//        System.out.println("Изменить членство в сборной города");
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        Select rank = new Select(driver.findElement(By.name("rankId")));
//        rank.selectByIndex(2);
//        js.executeScript("window.scrollBy(0,300)");
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.id("btn"));
//        btn.click();
//    }
//
//    @Order(4)
//    @Test
//    public void dTest() throws InterruptedException {
//        System.out.println("Удаление спортсмена");
//        this.driver.get(this.url);
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.className("btn-danger"));
//        btn.click();
//    }
//
//    @Order(5)
//    @Test
//    public void eTest() throws InterruptedException {
//        System.out.println("Удаление персоны");
//        this.driver.get(this.personUrl);
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.className("btn-danger"));
//        btn.click();
//    }
//}
