//package kz.attractorschool.gymnasticsfederation;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//public class FirstModuleTest {
//    private WebDriver driver;
//
//    @BeforeTest
//    public void setProperty() throws InterruptedException {
//        Thread.sleep(1500);
//        System.setProperty("webdriver.chrome.driver", "/root/chromedriver/chromedriver");
//        this.driver = new ChromeDriver();
//    }
//
//    @AfterTest
//    public void quit(){
//        System.out.println("Тест окончен");
//        this.driver.quit();
//    }
//
//    @Test
//    public void addFederationTest() throws InterruptedException {
//        System.out.println("Добавление федерации");
//        this.driver.get("http://localhost:8001/federation");
//        Thread.sleep(2000);
//        driver.findElement(By.name("name")).sendKeys("federationTest");
//        driver.findElement(By.name("director")).sendKeys("dir");
//        driver.findElement(By.name("address")).sendKeys("addressTest");
//        driver.findElement(By.name("phone")).sendKeys("phoneTest");
//        driver.findElement(By.name("email")).sendKeys("emailTest@test.ru");
//        driver.findElement(By.name("password")).sendKeys("passwordTest");
//        driver.findElement(By.id("btn")).click();
//        assertTrue(driver.findElement(By.id("error")).getText().contains("Ведите ФИО директора"));
//        driver.findElement(By.name("director")).sendKeys("directorTest");
//        driver.findElement(By.id("btn")).click();
//    }
//
//    @Test
//    public void addSchoolTest() throws InterruptedException {
//        System.out.println("Добавление школы");
//        this.driver.get("http:/localhost:8001/school");
//        Thread.sleep(2000);
//        WebElement federation = driver.findElement(By.name("federationId"));
//        Select select = new Select(federation);
//        Thread.sleep(1000);
//        driver.findElement(By.name("name")).sendKeys("schoolTest");
//        driver.findElement(By.name("director")).sendKeys("directorTest");
//        driver.findElement(By.name("address")).sendKeys("addressTest");
//        driver.findElement(By.name("phone")).sendKeys("phoneTest");
//        driver.findElement(By.name("email")).sendKeys("emailTest@test.ru");
//        driver.findElement(By.name("password")).sendKeys("passwordTest");
//        Thread.sleep(1000);
//        select.selectByIndex(1);
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.id("btn"));
//        btn.click();
//    }
//
//    @Test
//    public void addPersonTest() throws InterruptedException {
//        System.out.println("Добавление персоны");
//        this.driver.get("http:/localhost:8001/person");
//        Thread.sleep(2000);
//        assertEquals("Добавить персону", driver.getTitle());
//        driver.findElement(By.name("name")).sendKeys("nameTest2");
//        driver.findElement(By.name("surname")).sendKeys("surnameTest2");
//        driver.findElement(By.name("surname")).sendKeys("middleNameTest2");
//        driver.findElement(By.name("birthday")).sendKeys("05-05-2009");
//        driver.findElement(By.name("iin")).sendKeys("989898989898");
//        driver.findElement(By.name("address")).sendKeys("addressTest");
//        driver.findElement(By.name("city")).sendKeys("cityTest");
//        driver.findElement(By.name("phone")).sendKeys("phoneTest");
//        driver.findElement(By.name("email")).sendKeys("emailTest@test.ru");
//        driver.findElement(By.name("education")).sendKeys("educationTest");
//        driver.findElement(By.name("file")).sendKeys("D:\\ic-user.png");
//        Thread.sleep(2000);
//        Select select = new Select(driver.findElement(By.name("gender")));
//        select.selectByValue("Мужской");
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,300)");
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.id("btn"));
//        btn.click();
//    }
//
//    @Test
//    public void addAthleteTest() throws InterruptedException {
//        System.out.println("Добавление спортсмена");
//        this.driver.get("http:/localhost:8001/athlete");
//        Thread.sleep(2000);
//        assertEquals("Добавить спортсмена", driver.getTitle());
//        Select person = new Select(driver.findElement(By.name("personId")));
//        person.selectByIndex(12);
//        Select school = new Select(driver.findElement(By.name("schoolId")));
//        school.selectByIndex(2);
//        Select discipline = new Select(driver.findElement(By.name("disciplineId")));
//        discipline.selectByIndex(2);
//        Select rank = new Select(driver.findElement(By.name("rankId")));
//        rank.selectByIndex(1);
//        driver.findElement(By.name("registryDate")).sendKeys("2020-05-05");
//        driver.findElement(By.name("registryFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        driver.findElement(By.name("rankFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        driver.findElement(By.name("medicalFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        driver.findElement(By.name("dopingFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        Select isNationalTeam = new Select(driver.findElement(By.name("isNationalTeam")));
//        isNationalTeam.selectByIndex(1);
//        Select isCityTeam = new Select(driver.findElement(By.name("isCityTeam")));
//        isCityTeam.selectByIndex(1);
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,300)");
//        Thread.sleep(2000);
//        WebElement btn = driver.findElement(By.id("btn"));
//        btn.click();
//    }
//}
