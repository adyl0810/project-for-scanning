//package kz.attractorschool.gymnasticsfederation;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import static org.junit.Assert.assertEquals;
//
//public class SecondModuleTest {
//    private WebDriver driver;
//
//    @BeforeTest
//    public void setProperty(){
//        System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver\\chromedriver.exe");
//        this.driver = new ChromeDriver();
//    }
//
//    @AfterTest
//    public void quit(){
//        this.driver.quit();
//    }
//
//    @Test
//    public void addCompetitionTest() throws InterruptedException {
//
//        System.out.println("Добавление соревнования");
//        this.driver.get("http:/localhost:8001/competitions/add");
//        Thread.sleep(2000);
//        assertEquals("Добавить соревнование", driver.getTitle());
//        driver.findElement(By.name("name")).sendKeys("competitionTest");
//        driver.findElement(By.name("startDate")).sendKeys("05-05-2022");
//        driver.findElement(By.name("finishDate")).sendKeys("15-05-2022");
//        driver.findElement(By.name("participationDate")).sendKeys("01-05-2022");
//        Select level = new Select(driver.findElement(By.name("level")));
//        level.selectByIndex(1);
//        driver.findElement(By.name("country")).sendKeys("countryTest");
//        driver.findElement(By.name("areaName")).sendKeys("areaNameTest");
//        driver.findElement(By.name("city")).sendKeys("cityTest");
//        driver.findElement(By.name("address")).sendKeys("addressTest");
//        Select discipline = new Select(driver.findElement(By.name("disciplineId")));
//        discipline.selectByIndex(2);
//        driver.findElement(By.name("contactName")).sendKeys("contactNameTest");
//        driver.findElement(By.name("contactPhone")).sendKeys("contactPhoneTest");
//        driver.findElement(By.name("competitionPositionFile")).sendKeys("D:\\test.pdf");
//        Thread.sleep(1000);
//        WebElement btn = driver.findElement(By.id("next_btn"));
//        Thread.sleep(2000);
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,400)");
//        Thread.sleep(2000);
//        btn.click();
//
//        System.out.println("Вторая часть добавления соревнования");
//        js.executeScript("window.scrollBy(0,-400)");
//        Thread.sleep(2000);
//        Select disciplineType = new Select(driver.findElement(By.name("disciplineTypeId")));
//        disciplineType.selectByIndex(1);
//        Thread.sleep(1500);
//        driver.findElement(By.id("discipline-type-1")).click();
//        driver.findElement(By.id("discipline-type-0")).click();
//        driver.findElement(By.id("age-category-0")).click();
//        driver.findElement(By.id("age-category-1")).click();
//
//        Thread.sleep(1000);
//        driver.findElement(By.id("save_program_btn")).click();
//
//        Thread.sleep(1000);
//        WebElement number = driver.findElement(By.cssSelector("input[type='number']"));
//
//        Thread.sleep(1000);
//        number.sendKeys("5");
//
//        driver.findElement(By.id("send-form-btn")).click();
//    }
//}
